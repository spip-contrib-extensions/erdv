<?php

/**
 * Supprimer un calendrier
 *
 * @plugin     Erdv
 * @copyright  2024
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Erdv\Actions
 **/

if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * Action pour supprimer un calendrier
 * 
 * @param null|int $arg
 *     `id` de l'eclandrier. En absence de `id`, utilise l'argument de l'action sécurisée.
**/
function action_supprimer_ecalendrier_dist($arg = null) {
	if (is_null($arg)){
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}
	$id_ecalendrier = intval($arg);

	if ($id_ecalendrier) {
		// supprimer le calendrier demandé
		sql_delete('spip_ecalendriers', 'id_ecalendrier=' . $id_ecalendrier);
		// supprimer les rendez-vous du calendrier
		sql_delete('spip_erdvs', 'id_ecalendrier=' . $id_ecalendrier);
		// supprimer les liaisons devenues orphelines de spip_auteurs_liens
		sql_delete('spip_auteurs_liens', [
				'id_objet=' . $id_ecalendrier,
				'objet=' .sql_quote('ecalendrier')
			]
		);
		spip_log("action_supprimer_ecalendrier_dist. Calendrier n°$id_ecalendrier supprimé par {$GLOBALS['visiteur_session']['nom']} (auteur n°{$GLOBALS['visiteur_session']['id_auteur']}).", 'erdv.' . _LOG_INFO_IMPORTANTE);
	}
	else {
		spip_log("action_supprimer_ecalendrier_dist $arg pas compris", 'erdv.' . _LOG_DEBUG);
	}
}