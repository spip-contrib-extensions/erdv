<?php

/**
 * Action pour supprimer un rendez-vous
 *
 * @plugin     Erdv
 * @copyright  2024
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Erdv\Actions
 **/


if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Action pour supprimer un rendez-vous
 *
 * @uses editer_objet()
 *
 * L'environnement sécurisé transmet :
 *                 $id_erdv
 * @return void
 */
function action_supprimer_erdv_dist($arguments = null) : void {
	// avoir les arguments
	if (null === $arguments) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arguments = $securiser_action();
	}

	// vérifier l'argument
	if (
		$id_erdv = (int) $arguments
		and include_spip('action/editer_objet')
	){
		// Pour la publication d’un objet (modification du statut ou de la date), 
		// il convient d’appeler la fonction objet_modifier 
		if ($erreur = objet_modifier(
				'erdv',
				$id_erdv,
				['statut' => 'poubelle']
			)
		){
			spip_log("Le statut de l’objet erdv n°$id_erdv n’a pas pu être basculer dans le statut poubelle.", 'erdv.' . _LOG_ERREUR);
		} else {
			// Invalider les caches
			include_spip('inc/invalideur');
			suivre_invalideur("id='erdv/$id_erdv'");
			// log en debug
			spip_log("Le statut de l’objet erdv n°$id_erdv a été basculer dans le statut poubelle avec succès.", 'erdv.' . _LOG_DEBUG);
		}
		
	}
}
