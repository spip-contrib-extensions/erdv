<?php

/**
 * Action pour faire appraitre en modale la liste des rendez-vous d'une journée
 *
 * L'utilité de cette présentation est de faciliter les copier-coller
 * qui sont complexes sur le tableau de ecalendrier_semaine 
 *
 * @plugin     Erdv
 * @copyright  2024
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Erdv\Action
 **/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Action pour afficher les rendez-vous d'une journée
 *
 * @uses editer_objet()
 *
 * L'environnement sécurisé transmet :
 *                 $id_ecalendrier int
 *                 $time int
 * @return void
 */
function action_visualiser_journee_dist($arguments = null) : void {
	// avoir les arguments
	if (null === $arguments) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arguments = $securiser_action();
	}
	// vérifier les arguments
	if ([$id_ecalendrier, $time] = explode('-', $arguments)){
		// sur quelle semaine est-on ?
		include_spip('inc/erdv');
		$semaine = semaine_en_cours($time);
		// demander les rendez-vous de la semaine
		$j = tetris_journees($semaine, $id_ecalendrier);
		// Envoyer aux plugins
		$j = pipeline(
			'ecalendrier_semaine',
			[
				'args' => [
					'type' => 'journees',
					'semaine' => $semaine,
					'id_ecalendrier' => $id_ecalendrier,
				],
				'data' => $j
			]
		);
		$h = tetris_heures($semaine, $id_ecalendrier);
		$h = pipeline(
			'ecalendrier_semaine',
			[
				'args' => [
					'type' => 'heures',
					'semaine' => $semaine,
					'id_ecalendrier' => $id_ecalendrier,
				],
				'data' => $h
			]
		);
		$correspondances = [
			'info_pour_la_journee' => $j,
			'info_par_heure' => $h
		];
		$listes = [
			'info_pour_la_journee' => '<p><em>Pour la journée</em></p>',
			'info_par_heure' => '<p><em>Par heure</em></p>'
		];
		$dj = date('N', $time) - 1; $texte['info_par_heure'] = ''; $texte['info_pour_la_journee'] = '';
		foreach ($correspondances as $label => $donnees) {
			foreach ($donnees as $ligne => $valeurs) {
				foreach ($valeurs as $jour => $valeur) {
					if (isset($valeur['titre'])){
						// retirer les éléments HTML
						$titre = strip_tags($valeur['titre']);
						if ($titre == ''){
							$titre = _T('sans_titre');
						}
					}
					if ($jour == $dj){
						// on a des heures
						if (isset($valeur['heure_debut']) and isset($valeur['heure_fin'])){
							$heures = $valeur['heure_debut'] . ' - ' . $valeur['heure_fin'] . ' ';
						} else {
							$heures = '';
						}
						// on est sur un élément sur plusieurs colonnes qui concerne le jour résumé
						if (is_string($valeur)){
							$texte[$label] .= "{$heures}{$titre}<br>";
						// on est sur un élément qui débute ou qui est limité au jour résumé
						} elseif (is_array($valeur) and isset($valeur['titre'])){
							$texte[$label] .= "{$heures}{$titre}<br>";
						}
					}
				}
			}
		}
		$i = 0; $message ='';
		foreach ($texte as $label => $txt) {
			if ($txt){
				$message .= $listes[$label] . '<p>' . $txt . '</p>';
			}
			$i++;
		}
		// afficher les rendez-vous du jour
		include_spip('inc/minipres');
		$redirect = generer_url_ecrire(
			'ecalendrier_semaine',
			"id_ecalendrier=$id_ecalendrier&date=" . date('Y-m-d',$time). ' 00:00:00',
			true
		);
		echo minipres(
			_T('erdv:titre_resume_journee'),
			$message,
			[
				'footer' => "<a href='$redirect'>Retour</a>",
				'status' => 200
			]
		);
		exit;
	} else {
		echo minipres(_T('info_acces_interdit'));
		return;
	}
}
