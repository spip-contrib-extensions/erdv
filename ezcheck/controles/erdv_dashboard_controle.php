<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Ce contrôle permet d'identifier les rendez-vous dont l'anonymisation est incomplète.
 *
 * L'incomplétude peut résulter d'un job inachevé ou abandonné. 
 * Note pour les développeuses et développeurs : 
 * Le traitement au sein de la fonction et des fonctions de correction est rédigé de façon agnostique 
 * pour être facilement réutilisables pour d'autres objets anonymisés que l'on voudrait contrôler.
 * L'initialisation demande un paramétrage (les valeurs transmises dans $options proviennent de 
 * la déclaration des parametres dans le YAML erdv_dashboard_erdvs_anonymisation.yaml).
 *
 * @uses inc/filtres.php, inc/ezcheck_observation.php
 *
 * @param int   $id_controle Identifiant du contrôle relatif à l'exécution de la fonction
 * @param int   $id_auteur   Identifiant de l'auteur ayant lancé le contrôle ou 0 si c'est un CRON
 * @param array $options     Paramètres nécessaires à l'exécution du contrôle
 *
 * @return string Erreur sous la forme d'une chaine (de type mot) ou vide si aucune erreur.
 */
function erdv_dashboard_erdvs_anonymisation(int $id_controle, int $id_auteur, array $options) : string {
	// Initialisation de l'erreur à chaine vide soit 'aucune erreur'.
	$erreur = '';
	$log = 'erdv.';

	// on acquière les champs anonymisés
	include_spip('inc/filtres');
	$spip_table_objet = table_objet_sql($options['objet_anonymise']);
	$trouver_table = charger_fonction('trouver_table', 'base');
	$desc = $trouver_table($spip_table_objet);
	$id_table_objet = id_table_objet($options['objet_anonymise']);
	$anonymisation = array_keys($desc['champs_anonymises']);
	$champs_anonymises = implode(',', $anonymisation);

	// on liste les rendez-vous
	if ($result = sql_select("$id_table_objet, $champs_anonymises", $spip_table_objet, "statut='anonyme'", '', $options['orderby'])){
		while ($row = sql_fetch($result)) {
			$id_ = $row[$id_table_objet];
			// Initialisation par défaut d'une anomalie pour le type de contrôle
			$anomalie = [
				'type_controle' => $options['type_controle'],
				'objet' => $options['objet_anonymise'],
				'id_objet' => $id_
			];
			foreach ($row as $cle => $valeur) {
				echo "$cle => $valeur<br>";
				if (isset($desc['champs_anonymises'][$cle])) {
					if ($desc['champs_anonymises'][$cle] == 'void' and $valeur != '') {
						echo "on génère une anomalie pour {$desc['champs_anonymises'][$cle]}<br>";
						// - on génère une anomalie
						$anomalie['code'] = 'column_void';
						// On en profite pour stocker dans les paramètres de l'anomalie
						// et la valeur qu'elle devrait avoir
						$anomalie['parametres'] = [
							'column' => $cle,
							'value' => ''
						];
					}
				}
			}
			// Si une anomalie a été détectée on l'ajoute dans la base
			if (!empty($anomalie['code'])) {
				// On crée l'anomalie
				include_spip('inc/ezcheck_observation');
				observation_ajouter(true, $id_controle, $anomalie);
			}
		}
	} else {
		// pas de rendez-vous anonymisé à vérifier, on log un debug exhaustif
		$requete = sql_get_select("$id_table_objet, $champs_anonymises", $spip_table_objet, "statut = 'anonyme'", '', $options['orderby']);
		spip_log("La requête ($requete) ne ramène pas d’objet anonymisé à vérifier par le Dashboard. Les options transmises sont : " . print_r($options, true), $log . _LOG_DEBUG);
	}
	return $erreur;
}
/**
 * Corriger la colonne qui n'a pas été vidée
 *
 * note : Les fonctions de correction possèdent un nom imposé 
 * composé de l’identifiant du type de contrôle (erdvs_anonymisation)
 * et de l’identifiant de l’anomalie à corriger (column_void)
 *
 * @uses action/editer_objet.php
 *
 * @param int   $id_observation Identifiant du contrôle relatif à l'observation
 * @param int   $id_auteur   Identifiant de l'auteur ayant lancé le contrôle ou 0 si c'est un CRON
 *
 * @return string Erreur sous la forme d'une chaine (de type mot) ou vide si aucune erreur.
 */
function erdv_dashboard_erdvs_anonymisation_column_void(int $id_observation, int $id_auteur) : string {
	// Initialisation de l'erreur à chaine vide soit 'aucune erreur'.
	$erreur = '';
	$log = 'erdv.';

	// Récupération des informations nécessaires sur l'observation
	include_spip('action/editer_objet');
	$observation = objet_lire('observation', $id_observation);

	// Le champ 'parametres' de l'anomalie contient
	// - la colonne problématique
	// - la valeur qu'elle devrait contenir.
	// -> on fait la modification
	$parametres = unserialize($observation['parametres']);
	if (!empty($parametres['column'])) {
		$maj_column = [
			$parametres['column'] => $parametres['value']
		];
	}
	// La fonction retourne une chaîne vide si tout s’est bien passé ou l’erreur en cas d’échec.
	if ($e = objet_modifier($observation['objet'], (int) ($observation['id_objet']), $maj_column)){
		$erreur = 'corr_nok';
		spip_log("Le Dashboard n’est pas parvenu à forcer l’anonymination du champs {$parametres['column']} de l’objet {$observation['objet']} n°{$observation['id_objet']}. " . $e, $log . _LOG_DEBUG);
	}
	return $erreur;
}

/**
 * Ce contrôle permet d'indentifier sur les dates des vacances scolaires sont bien indiquées 
 * dans les ecalendriers
 *
 * L'initialisation demande un paramétrage (les valeurs transmises dans $options proviennent de 
 * la déclaration des parametres dans le YAML erdv_dashboard_calendriers_vacances.yaml).
 *
 * @uses inc/requeter_calendrier.php,
 *
 * @param int   $id_controle Identifiant du contrôle relatif à l'exécution de la fonction
 * @param int   $id_auteur   Identifiant de l'auteur ayant lancé le contrôle ou 0 si c'est un CRON
 * @param array $options     Paramètres nécessaires à l'exécution du contrôle
 *
 * @return string Erreur sous la forme d'une chaine (de type mot) ou vide si aucune erreur.
 */
function erdv_dashboard_calendriers_vacances(int $id_controle, int $id_auteur, array $options) : string {
	// Initialisation de l'erreur à chaine vide soit 'aucune erreur'.
	$erreur = '';
	$log = 'erdv.';
	// Quelles sont les zones ? (to do : ajouter les zones particulières)
	$correspondances = [
		'zone_c' => 'zones:"Zone C"',
		'location_zone_c' => 'location:"Paris"', // Créteil Versailles Montpellier Toulouse
		'zone_a' => 'zones:"Zone A"',
		'location_zone_a' => 'location:"Lyon"', // Besançon Bordeaux Clermont-Ferrand Dijon Grenoble Limoges Poitiers 
		'zone_b' => 'zones:"Zone B"',
		'location_zone_b' => 'location:"Lille"', // Aix-Marseille Amiens Nancy-Metz Nantes Nice...
	];
	// quelles sont les années scolaires ?
	$das = ( \date_create()->format('Y') - (\date_create()->format('m') < '09' ? 1 : 0) );
	$fas = (1 + ( \date_create()->format('Y') - (\date_create()->format('m') < '09' ? 1 : 0) ));
	$correspondances['annee_scolaire_en_cours'] = 'annee_scolaire:"' . $das . "-" . $fas .'"';
	$correspondances['annee_scolaire_suivante'] = 'annee_scolaire:"' . ($das + 1) . "-" . ($fas + 1) .'"';

	// Initialisation pour l'appel de l'API externe
	$url_base ='https://data.education.gouv.fr/api/explore/v2.1/catalog/datasets';
	$dataset = 'fr-en-calendrier-scolaire';
	$taille_max = 0;
	// Définir les filtres en fonction de ce que l'utilisateur du Dashboard demande
	$filtres['limit'] = 20;
	foreach ($options['zones'] as $nbr => $_zone) {
		$filtres['refine'][] = $correspondances[$_zone];
		$filtres['refine'][] = $correspondances["location_{$_zone}"];
	}
	foreach ($options['annees_scolaires'] as $nbr => $_annee_scolaire) {
		$filtres['refine'][] = $correspondances[$_annee_scolaire];
	}
	// s'assurer que l'on ne requête pas les dates antérieurs à la date d'archivage
	include_spip('inc/config');
	$delai = lire_config('erdv/delai_pour_archiver', '-1 month');
	$delai_pour_archiver = date('Y-m-d H:i:s', strtotime($delai, time() ) );
	//inclure une condition where=start_date>date'2024/04/01'
	$filtres['where'][] = 'start_date>\'' . $delai_pour_archiver . '\'';
	// Requête auprès de l'API de l'Éducation nationale
	if (
		($requeter = charger_fonction('requeter_calendrier','inc'))
		&& ($resultat = $requeter($url_base, $dataset, $filtres, [], $taille_max))
	){
		// La structuration retournée est la suivante :
		// [total_count] => int, nombre de résultats
		// [results] => array tableau numérique avec les valeurs : description, population -|Élèves|Enseignants ...
		foreach ($resultat['results'] as $cle => $valeurs) {
			// créer les informations pour le rendez-vous :
			// Le départ en vacances a lieu après la classe des jours indiqués.
			// La reprise des cours s'effectue le matin des jours indiqués.
			// Un affichage en journée pleine avec erdv demande d'ajuster
			foreach ($options['ecalendriers'] as $nbr => $id_ecalendrier) {
				if ($valeurs['population'] <> 'Enseignants'){
					$date_debut = date('Y-m-d H:i:s', strtotime(substr($valeurs['start_date'], 0, 10) . ' + 1 day'));
					$date_fin = date('Y-m-d H:i:s', strtotime(substr($valeurs['end_date'], 0, 10) . ' - 1 day'));
					$titre = "{$valeurs['description']} ({$valeurs['zones']})";
					$champs_erdv = [
						'id_erdv' => '',
						'id_ecalendrier' => $id_ecalendrier,
						'date_debut' => $date_debut,
						'date_fin' => $date_fin,
						'toute_la_journee' => 'oui',
						'recurrence_jour' => '0',
						'recurrence_semaine' => '0',
						'titre' => $titre,
						'description' => _T('erdv_dashboard:info_vacances_scolaires') . ' ' . $valeurs['annee_scolaire'],
						'statut' => 'publie',
						'class' => $options['class'],
						'maj' => date('Y-m-d H:i:s'),
					];
				} else {
				// n'indiquer que la rentrée des enseignants (qui se superpose aux vacances des élèves)
					$date_debut = date('Y-m-d H:i:s', strtotime(substr($valeurs['end_date'], 0, 10)));
					$date_fin = date('Y-m-d H:i:s', strtotime(substr($valeurs['end_date'], 0, 10)));
					$titre = _T('erdv_dashboard:info_rentree_des_enseignants') . ' (' . $valeurs['zones'] . ')';
					$champs_erdv = [
						'id_erdv' => '',
						'id_ecalendrier' => $id_ecalendrier,
						'date_debut' => $date_debut,
						'date_fin' => $date_fin,
						'toute_la_journee' => 'oui',
						'recurrence_jour' => '0',
						'recurrence_semaine' => '0',
						'titre' => $titre,
						'description' => $valeurs['description'] . ' ' . $valeurs['annee_scolaire'],
						'statut' => 'publie',
						'class' => $options['class'],
						'maj' => date('Y-m-d H:i:s'),
					];
				}
				// vérifier si le rendez-vous existe (titre et date)
				if (!$id_erdv = sql_getfetsel('id_erdv', 'spip_erdvs', [
					"id_ecalendrier=$id_ecalendrier",
					'date_debut=' . sql_quote($date_debut),
					'date_fin=' . sql_quote($date_fin),
					'titre=' . sql_quote($titre),
				])){
				// On crée l'anomalie (et champs_erdv permettra de la corriger)
					include_spip('inc/filtres');
					$description_normalisee = identifiant_slug($valeurs['description']);
					$anomalie['code'] = "log_abs_vac";
					
					$anomalie['objet'] = 'ecalendrier';
					$anomalie['id_objet'] = $id_ecalendrier;
					// On en profite pour stocker dans les paramètres de l'anomalie
					// et la valeur qu'elle devrait avoir
					$anomalie['parametres'] = [
						'cols' => $champs_erdv,
						'type' => $valeurs['description'],
						'zone' => $valeurs['zones'],
						'annee' => $valeurs['annee_scolaire'],
					];
					include_spip('inc/ezcheck_observation');
					observation_ajouter(true, $id_controle, $anomalie);
				}
			}
		}
	} else {
		// pas de requete possible, on log un debug exhaustif
		spip_log("La requête est impossible : " . print_r($resultat, true), $log . _LOG_DEBUG);
	}

	return $erreur;
}


/**
 * Fonction pour corriger l'absence d'indication des vacances scolaires
 *
 * note : Les fonctions de correction possèdent un nom imposé 
 * composé de l’identifiant du type de contrôle (calendriers_vacances)
 * et de l’identifiant de l’anomalie à corriger (log_abs_vac)
 * le type de l'observation permet de préciser de type de l'anomalie
 * @uses action/editer_objet.php
 *
 * @param int   $id_observation Identifiant du contrôle relatif à l'observation
 * @param int   $id_auteur   Identifiant de l'auteur ayant lancé le contrôle ou 0 si c'est un CRON
 *
 * @return string Erreur sous la forme d'une chaine (de type mot) ou vide si aucune erreur.
 */
function erdv_dashboard_calendriers_vacances_log_abs_vac(int $id_observation, int $id_auteur) : string {
	// Initialisation de l'erreur à chaine vide soit 'aucune erreur'.
	$erreur = '';
	$log = 'erdv.';

	// Récupération des informations nécessaires sur l'observation
	include_spip('action/editer_objet');
	$observation = objet_lire('observation', $id_observation);

	// Le champ 'parametres' de l'anomalie contient
	// - les champs de l'objet erdv à insérer.
	// -> on fait l'insertion
	$parametres = unserialize($observation['parametres']);
	$cols = $parametres['cols'];
	unset ($cols['id_erdv']);

	if (
		!empty($cols)
		&& ($e = objet_inserer('erdv', '', $cols))
	) {
		// La fonction objet_inserer retourne l’id de l’enregistrement ajouté en base, ou 0 en cas d’échec.
		spip_log("Le Dashboard est parvenu à créer un rendez-vous n°$e", $log . _LOG_DEBUG);
		
	} else {
		$erreur = 'corr_nok';
		if (isset($e)){
		$message = $e . '. ';
		} else {
		$message = '';
		}
		$message .= 'Le Dashboard n’est pas parvenu à créer un rendez-vous avec les paramètres :' . print_r($parametres, true);
		spip_log($message, $log . _LOG_DEBUG);
	}
	return $erreur;
}

/**
 * Ce contrôle permet d'indentifier si les dates jours fériés sont bien indiqués 
 * dans les ecalendriers
 *
 * L'initialisation demande un paramétrage (les valeurs transmises dans $options proviennent de 
 * la déclaration des parametres dans le YAML erdv_dashboard_jours_feries.yaml).
 *
 * @uses inc/requeter_calendrier.php,
 *
 * @param int   $id_controle Identifiant du contrôle relatif à l'exécution de la fonction
 * @param int   $id_auteur   Identifiant de l'auteur ayant lancé le contrôle ou 0 si c'est un CRON
 * @param array $options     Paramètres nécessaires à l'exécution du contrôle
 *
 * @return string Erreur sous la forme d'une chaine (de type mot) ou vide si aucune erreur.
 */
function erdv_dashboard_jours_feries(int $id_controle, int $id_auteur, array $options) : string {
	// Initialisation de l'erreur à chaine vide soit 'aucune erreur'.
	$erreur = '';
	$log = 'erdv.';
	// Quelles est la zone ou les zones?
	$zones = $options['zones'];
	$ecalendriers = $options['ecalendriers'];
	// traduire les années en chiffre
	$das = date('Y');
	$correspondances['annee_en_cours'] = $das;
	$correspondances['annee_suivante'] = ($das + 1);
	$annees = [];
	foreach ($options['annees'] as $nbr => $_annee) {
		$annees[] = $correspondances[$_annee];
	}

	// Initialisation pour l'appel de l'API externe
	$url_base ='https://calendrier.api.gouv.fr/jours-feries';
	$filtres = [];
	$taille_max = 0;

	// L'api ne connait par d'argument where.
	// s'assurer que l'on ne requête pas les dates antérieurs à la date d'archivage
	include_spip('inc/config');
	$delai = lire_config('erdv/delai_pour_archiver', '-1 month');
	$delai_pour_archiver = date('Y-m-d', strtotime($delai, time() ) );

	// pour chaque annee
	foreach ($annees as $cle => $_annee) {
		// pour chaque zone
		foreach ($zones as $c => $_zone) {
			$dataset = "{$_zone}/{$_annee}.json";
			// Requête auprès de l'API gouvernementale
			if (
				($requeter = charger_fonction('requeter_calendrier','inc'))
				&& ($resultat = $requeter($url_base, $dataset, $filtres, [], $taille_max))
			){
				// La structuration retournée est la suivante :
				// "2025-01-01" => "Jour de l'an",
				foreach ($resultat as $date => $libelle) {
					// vérifier que vérifier est utile...
					if ($date > $delai_pour_archiver){
						// créer le rendez-vous pour chaque calendrier demandé
						$date_debut = date('Y-m-d H:i:s', strtotime($date));
						$date_fin = substr($date_debut, 0, 10).' 23:59:59';
						foreach ($ecalendriers as $nbr => $id_ecalendrier) {
							$champs_erdv = [
								'id_erdv' => '',
								'id_ecalendrier' => $id_ecalendrier,
								'date_debut' => $date_debut,
								'date_fin' => $date_fin,
								'toute_la_journee' => 'oui',
								'recurrence_jour' => '0',
								'recurrence_semaine' => '0',
								'titre' => $libelle,
								'description' => "{$_zone}/{$_annee}",
								'statut' => 'publie',
								'class' => $options['class'],
								'maj' => date('Y-m-d H:i:s'),
							];
							spip_log($champs_erdv, $log . _LOG_DEBUG);
						}
						// vérifier si le rendez-vous existe (titre et date)
						if (!$id_erdv = sql_getfetsel('id_erdv', 'spip_erdvs', [
							"id_ecalendrier=$id_ecalendrier",
							'date_debut=' . sql_quote($date_debut),
							'date_fin=' . sql_quote($date_fin),
							'titre=' . sql_quote($libelle),
							'toute_la_journee=' . sql_quote('oui'),
						])){
						// On crée l'anomalie (et champs_erdv permettra de la corriger)
							$anomalie['code'] = "log_abs_fer";
							$anomalie['objet'] = 'ecalendrier';
							$anomalie['id_objet'] = $id_ecalendrier;
							// On en profite pour stocker dans les paramètres de l'anomalie
							// et la valeur qu'elle devrait avoir
							$anomalie['parametres'] = [
								'cols' => $champs_erdv,
								'libelle' => $libelle,
								'zone' => $_zone,
								'date' => date('d/m/Y', strtotime($date)),
							];
							include_spip('inc/ezcheck_observation');
							observation_ajouter(true, $id_controle, $anomalie);
						}
					}
				}
			} else {
				// pas de requete possible, on log un debug exhaustif
				spip_log("La requête est impossible : " . print_r($resultat, true), $log . _LOG_DEBUG);
			}
		}
	}
	return $erreur;
}

/**
 * Fonction pour corriger l'absence d'indication d'un jour férié
 *
 * note : Les fonctions de correction possèdent un nom imposé 
 * composé de l’identifiant du type de contrôle (jours_feries)
 * et de l’identifiant de l’anomalie à corriger (log_abs_fer)
 * le type de l'observation permet de préciser de type de l'anomalie
 * note 2 : la présente fonction se contente d'appeler la fonction 
 * erdv_dashboard_calendriers_vacances_log_abs_vac 
 * @uses action/editer_objet.php
 *
 * @param int   $id_observation Identifiant du contrôle relatif à l'observation
 * @param int   $id_auteur   Identifiant de l'auteur ayant lancé le contrôle ou 0 si c'est un CRON
 *
 * @return string Erreur sous la forme d'une chaine (de type mot) ou vide si aucune erreur.
 */
function erdv_dashboard_jours_feries_log_abs_fer(int $id_observation, int $id_auteur) : string {
	return erdv_dashboard_calendriers_vacances_log_abs_vac($id_observation, $id_auteur);
}