<?php

return [
    'erdv' => 'Les rendez-vous',
    'erdv_statut' => 'Les statuts d’un rendez-vous',
    'erdv_type' => 'Les types de rendez-vous',
    'erdvauteurs' => 'Les autrices et auteurs du rendez-vous',
];
