<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [
	'erdv_dashboard_erdvs_anonymisation_column_void' => 'Une colonne n’est pas vidée !',
	'erdv_dashboard_calendriers_vacances_log_abs_vac' => 'Vacances non indiquées : @annee@ - @zone@ - @type@',
	'erdv_dashboard_jours_feries_log_abs_fer' => 'Jour férié non indiqué pour la zone @zone@ : @date@ - @libelle@',
];