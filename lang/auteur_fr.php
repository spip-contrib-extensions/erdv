<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [
	'info_erdv_aucun_auteur' => 'Aucune personne organisatrice',
	'info_erdv_1_auteur' => '1 personne organisatrice',
	'info_erdv_nb_auteurs' => '@nb@ personnes organisatrices',
];
