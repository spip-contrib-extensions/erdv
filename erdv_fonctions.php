<?php
/**
 * Fonctions (générales) utiles au plugin
 *
 * @plugin     Erdv
 * @copyright  2024
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Erdv\Installation
 */
 
 
if (!defined('_ECRIRE_INC_VERSION')) return;
 
/**
 * Filtre pour avoir la liste des calendriers auxquels à accès un auteur
 *
 * Les filtres de SPIP sont des fonctions PHP qui reçoivent la balise sur laquelle ils sont appliqués
 * en premier paramètre et retournent les éléments à afficher.
 * @exemple `#SESSION{id_auteur}|liste_cal_auteur`
 *
 * @uses charger_fonction, liste_ecalendriers_auteur
 * 
 * @param int $id_auteur
 * @return array
 */
function liste_cal_auteur(int $id_auteur) : array {
	if ($calendriers = charger_fonction('liste_ecalendriers_auteur','inc', true)) {
		return $calendriers($id_auteur);
	}
	return [];
}