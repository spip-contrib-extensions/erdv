<?php
/**
 * Filtres pour le squelette ecalendrier_semaine.html
 *
 * @plugin     erdv
 * @copyright  2024
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Erdv\x
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Définir la semaine
 *
 * @param array $semaine
 * @param int $id_ecalendrier
 * @return array
 */
function definir_semaine(int $time) : array {
	// Inclure les fichiers contenant les classes
	include_spip('inc/erdv');
	// Utilisation de la classe
	$semaineEnCours = new SemaineEnCours($time);
	return $semaineEnCours->getSemaine();
}


/**
 * Récupère et organise les rendez-vous pour une semaine donnée, puis applique un pipeline de traitement.
 *
 * Un tétris à l'envers : on tente de placer les rendez-vous
 * sur la première ligne, et si ce n'est pas possible,
 * on descend.
 *
 * @param array $semaine Tableau contenant les dates de la semaine.
 * @param int $id_ecalendrier Identifiant du calendrier.
 * @return array Tableau contenant les rendez-vous organisés et traités par le pipeline.
 */
function tetris_j(array $semaine, int $id_ecalendrier) : array {
	// Inclure les fichiers contenant les classes
	include_spip('inc/erdv');
	// Créer une instance de la classe TetrisJournees
	$tetris = new TetrisJournees();
	// Appeler la méthode tetris pour récupérer et organiser les rendez-vous
	$champs = $tetris->tetris($semaine, $id_ecalendrier);

	// Envoyer aux plugins
	$champs = pipeline(
		'ecalendrier_semaine',
		[
			'args' => [
				'type' => 'journees',
				'semaine' => $semaine,
				'id_ecalendrier' => $id_ecalendrier,
			],
			'data' => $champs
		]
	);
	return $champs;
}

/**
 * Consolider les rendez-vous par heure
 *
 * En colonne
 * to do : en ligne aussi
 *
 * @param array $semaine
 * @param int $id_ecalendrier
 * @return array
 */
function tetris_h(array $semaine, int $id_ecalendrier) : array {
	// Inclure les fichiers contenant les classes
	include_spip('inc/erdv');
	// Créer une instance de la classe TetrisHeures
	$tetrisHeures = new TetrisHeures();
	// Appeler la méthode consolider pour consolider les rendez-vous par heure
	$champs = $tetrisHeures->consolider($semaine, $id_ecalendrier);
	// Envoyer aux plugins
	$champs = pipeline(
		'ecalendrier_semaine',
		[
			'args' => [
				'type' => 'heures',
				'semaine' => $semaine,
				'id_ecalendrier' => $id_ecalendrier,
			],
			'data' => $champs
		]
	);
	return $champs;
}

/**
 * Consolider les rendez-vous préformatés
 *
 * @param array $tableau
 * @return array
 */
function compacter_erdv(array $tableau) {
	// Inclure les fichiers contenant les classes
	include_spip('inc/erdv');
	// Créer une instance de la classe ShakeItUp
	$shakeItUp = new ShakeItUp();
	// Appeler la méthode consolider pour consolider pour inclure les rendez-vous
	return $shakeItUp->consolider($tableau);
}