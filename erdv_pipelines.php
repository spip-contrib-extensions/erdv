<?php
/**
 * Utilisations de pipelines par le plugin
 *
 * @plugin     Erdv
 * @copyright  2024
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Erdv\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Utiliser ce pipeline permet d'ajouter du contenu sur certaines pages,
 * notamment des formulaires de liaisons entre objets
 *
 * ici, le plugin :
 * - permet de choisir un calendrier comme celui par défaut
 * - permet l'association des auteurs aux calendriers,
 *   car eux seul peuvent les voir et y créer des rendez-vous.
 * - permet d'ajouter ou de retirer un auteur d'un rendez-vous.
 *
 * @pipeline affiche_milieu
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function erdv_affiche_milieu($flux) {
	$texte = '';
	# la configuration des préférences de l'auteur
	if (
		isset($flux['args']['exec'])
		&& $flux['args']['exec'] == 'configurer_preferences'
	) {
		$flux['data'] .= recuperer_fond('prive/squelettes/inclure/inc-configurer_preferences_ecalendrier');
	}
	# choisir son calendrier par défaut
	if (isset($flux['args']['exec']) and $e = trouver_objet_exec($flux['args']['exec']) 
		and isset($e['type']) and $type = $e['type'] 
		and isset($e['id_table_objet']) and isset($flux['args'][$e['id_table_objet']]) and $id = intval($flux['args'][$e['id_table_objet']])
	){

		# auteurs sur erdv
		if (!$e['edition'] and $type == 'erdv') {
			if (include_spip('inc/autoriser')){
				$texte .= recuperer_fond(
					'prive/objets/editer/liens',
					[
						'table_source'=>'auteurs',
						'objet' => $type,
						'id_objet' => $id,
						'editable' => autoriser('associerauteurs', $e['type'], $id) ? 'oui' : 'non'
					]
				);
			}
		}

		# auteurs sur ecalendrier
		if (!$e['edition'] and $type == 'ecalendrier') {
			$texte .= recuperer_fond(
				'prive/objets/editer/liens',
				array(
					'table_source'=>'auteurs',
					'objet' => $type,
					'id_objet' => $id,
				#'editable'=>autoriser('associerauteurs',$e['type'],$e['id_objet'])?'oui':'non'
				)
			);
		}

		if ($texte) {
			if ($p = strpos($flux['data'], '<!--affiche_milieu-->') )
				$flux['data'] = substr_replace($flux['data'],$texte,$p,0);
			else
				$flux['data'] .= $texte;
		}
	}

	return $flux;
}

/**
 * Utiliser ce pipeline permet d'ajouter des information sur la colonne gauche,
 *
 * @pipeline affiche_gauche
 * @use find_all_in_path qui retourne un tableau ayant pour clé le nom du fichier et pour valeur le nom du fichier précédé par son chemin
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function erdv_affiche_gauche($flux) {
	
	return $flux;
}

/**
 * Utiliser ce pipeline permet d'ajouter une aide au plugin,
 *
 * @link https://contrib.spip.net/Creer-une-aide-en-ligne-pour-un-plugin
 * @pipeline aide_index
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function erdv_aide_index($index) {
	$index['erdv'] = [
		'statut',
		'type'
	];
	// aide pour l'association d'un auteur ou d'une autrice à un rendez-vous
	$index['erdvauteurs'] = [];

	return $index;
}

/**
 * Ajouter les objets sur les vues des parents directs
 *
 * ici, le plugin :
 * - permet la création de rendez-vous dans le calendrier.
 *
 * @pipeline affiche_enfants
 * @uses API autoriser, API presentation
 *       pour les demandes d'autorisation et l'affichage des icone_verticales
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
**/
function erdv_affiche_enfants($flux) {

	if ($e = trouver_objet_exec($flux['args']['exec']) and $e['edition'] == false) {
		$id_objet = $flux['args']['id_objet'];

		if (
			$e['type'] == 'ecalendrier'
			and include_spip('inc/autoriser')
			and autoriser('creererdvdans', 'ecalendrier', $id_objet)
			and include_spip('inc/presentation')
		) {
				$flux['data'] .= icone_verticale(
					_T('erdv:icone_creer_erdv'),
					generer_url_ecrire('erdv_edit', "id_erdv=new&id_ecalendrier=$id_objet"),
					'erdv-24.png',
					'new',
					'right'
				) . "<br class='nettoyeur' />";
		}
	}
	return $flux;
}

/**
 * Ce pipeline est appelé après la création d'un nouvel objet. 
 *
 * ici, le plugin :
 * - permet de placer une adresse par défaut aux rendez-vous.
 *
 * @pipeline post_insertion
 * @link https://programmer.spip.net/post_insertion
 *
 * @param  array $flux Données du pipeline
 *               un tableau comportant 2 clés :
 *               ['data'] contient les champs et leurs valeurs insérées.
 *               ['args'] contient le nom de la table ['table'] et l’identifiant unique créé ['id_objet'].
 * @return array       Données du pipeline
 */
function erdv_post_insertion($flux) {

	if( isset($flux['args']['table']) and $table_objet_sql = $flux['args']['table']
		and ( 
				( isset($flux['args']['type']) and $objet = $flux['args']['type'] )
				or 
				( $objet = objet_type($table_objet_sql) )
			)
		and isset($flux['args']['id_objet']) and $id = intval($flux['args']['id_objet'])
	){
		if (
			$objet == 'erdv'
			and test_plugin_actif('coordonnees')
			and $id_ecalendrier = (int) _request('id_ecalendrier')
			and include_spip('inc/config')
			and lire_config('erdv/lieu_du_calendrier','oui') == 'oui'
			and $id_adresse = sql_getfetsel(
				'a.id_adresse',
				[
					'spip_adresses as a',
					'spip_adresses_liens as l'
				],
				[
					'l.id_objet=' . $id_ecalendrier,
					'l.objet="ecalendrier"',
					'l.type="pref"',
					'a.id_adresse=l.id_adresse'
				]
			)
			
		){
			# placer par défaut sur le rendez-vous l'adresse préférée du calendrier
			sql_insertq( 'spip_adresses_liens', [
				'id_adresse' => $id_adresse,
				'objet' => $objet,
				'id_objet' => $id,
				'type' => 'pref'
				]
			);
		}
	}
	return $flux;
}

/**
 * Optimiser la base de données
 *
 * Appelé depuis ecrire/genie/optimiser.php, ce pipeline permet de compléter le nettoyage des objets 
 * en supprimant des éléments lors des tâches périodiques.
 *
 * ici, le plugin :
 * - supprimer les rendez-vous et calendriers placés à la poubelle
 * - supprimer les liaisons orphelines des rendez-vous et calendriers
 * - archiver périodiquement les rendez-vous
 * - anonymiser périodiquement les rendez-vous
 *
 * @pipeline optimiser_base_disparus
 * @link https://programmer.spip.net/optimiser_base_disparus,532
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function erdv_optimiser_base_disparus($flux) {
	if (isset($flux['args']['date'])){ 
		$mydate = sql_quote(trim($flux['args']['date'], "'"));
		
		// supprimer des tables spip_OBJETs les objets à la poubelle
		sql_delete('spip_erdvs', "statut='poubelle' AND maj < $mydate");
		sql_delete('spip_ecalendriers', "statut='poubelle' AND maj < $mydate");
		
		// archiver les rendez-vous
		include_spip('inc/config');
		$delai = lire_config('erdv/delai_pour_archiver', '-1 month');
		$delai_pour_archiver = date('Y-m-d H:i:s', strtotime($delai, strtotime($flux['args']['date'])));
		if ($res = sql_select(['id_erdv'], ['spip_erdvs'], [
				'date_fin < ' . sql_quote($delai_pour_archiver),
				'statut="publie"'
			])
			and include_spip('action/editer_objet')
			and include_spip('inc/autoriser')
		){
			$i = 0;
			while ($row = sql_fetch($res)) {
				// La fonction objet_modifier appelle 
				// automatiquement les pipelines pre_edition et post_edition
				// ce qui permet aux autres plugins d'interagir
				// mais elle demande à ce que l'on accorde une autorisation exceptionnelle
				// afin d'effectuer cette tâche anonyme
				autoriser_exception('modifier', 'erdv', $row['id_erdv']);
				if ($erreur = objet_modifier('erdv', $row['id_erdv'], ['statut' => 'archive'])){
					spip_log("Erreur lors de la tentative d'archivage du rendez-vous n°{$row['id_erdv']} (posterieur à $delai_pour_archiver) : " . $erreur, 'genie.' . _LOG_ERREUR);
				} else {
					$i++;
				}
				autoriser_exception('modifier', 'erdv', $row['id_erdv'], false);
				// pas de traitement utile
				// $archiver_objet = charger_fonction('archiver_objet', 'inc');
				// $archiver_objet($row['id_erdv'], 'erdv');
			}
			spip_log("Nombre de rendez-vous archivés (posterieur à $delai_pour_archiver) : " . $i, 'genie.' . _LOG_INFO_IMPORTANTE);
		}

		// anonymiser les rendez-vous
		$delai = lire_config('erdv/delai_pour_anonymiser', '-3 month');
		$delai_pour_anonymiser = date('Y-m-d H:i:s', strtotime($delai, strtotime($flux['args']['date'])));
		if (
			$res = sql_select(['id_erdv'], ['spip_erdvs'], [
				'date_fin < ' . sql_quote($delai_pour_anonymiser),
				'statut="archive"'
			])
			&& include_spip('action/editer_objet')
			&& include_spip('inc/autoriser')
		){
			$i = 0;
			while ($row = sql_fetch($res)) {
				// même processus que ci-dessus pour l'archivage
				autoriser_exception('modifier', 'erdv', $row['id_erdv']);
				if ($erreur = objet_modifier('erdv', $row['id_erdv'], ['statut' => 'anonyme'])){
					spip_log("Erreur lors de la tentative d'anonymiser le rendez-vous n°{$row['id_erdv']} (posterieur à $delai_pour_anonymiser) : " . $erreur, 'genie.' . _LOG_ERREUR);
				}
				autoriser_exception('modifier', 'erdv', $row['id_erdv'], false);
				if (empty($erreur)){
					// traitement
					$anonymiser_objet = charger_fonction('anonymiser_objet', 'inc');
					$retours = $anonymiser_objet($row['id_erdv'], 'erdv');
					if (isset($retours['ok']) and $retours['ok'] === false){
						spip_log("Erreur lors du traitement de l’anonymisation du rendez-vous n°{$row['id_erdv']} (posterieur à $delai_pour_anonymiser)." . print_r($retours, true), 'genie.' . _LOG_ERREUR);
					} else {
						$i++;
					}
				}
			}
			spip_log("Nombre de rendez-vous anonymisés (posterieur à $delai_pour_anonymiser) : " . $i, 'genie.' . _LOG_INFO_IMPORTANTE);
		}
	}

	include_spip('action/editer_liens');
	// supprimer les liaisons orphelines des spip_OBJETs_liens
	$flux['data'] += objet_optimiser_liens(['erdv' => '*'], '*');
	$flux['data'] += objet_optimiser_liens(['ecalendrier' => '*'], '*');

	return $flux;
}
/**
 * Pipeline apportant les données avant l'affichage du planning de la semaine
 *
 * Ce pipeline permet de compléter l'affichage du planning
 * en supprimant, complétant ou ajoutant des éléments.
 *
 * @pipeline ecalendrier_semaine
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function erdv_ecalendrier_semaine($flux) {
	return $flux;
}

/**
 * Afficher des infos dans la boite de l'objet
 *
 * @pipeline boite_infos
 * @link : https://programmer.spip.net/boite_infos
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
**/
function erdv_boite_infos($flux) {
	if (isset($flux['args']['type'])
		&& ($objet = $flux['args']['type']) 
		&& isset($flux['args']['id'])
		&& ($id = intval($flux['args']['id']))
	){
		$texte = '';

		# la boite infos d'un rendez-vous
		if (
			$objet == 'erdv'
			&& $id
		){
			// connaitre le calendrier et la date du rendez-vous
			if (
				include_spip('action/editer_objet')
				&& ($champs = objet_lire('erdv', $id))
				&& ($url = generer_url_ecrire('ecalendrier_semaine', 'id_ecalendrier=' . $champs['id_ecalendrier'] .'&date=' . date('Y-m-d',strtotime($champs['date_debut']) ) ))
				&& include_spip('inc/presentation')
			) {
			// permettre la visualisation du calendrier de la semaine
				$texte = icone_horizontale(
					_T('ecalendrier:texte_voir_planning'),
					$url,
					'ecalendrier_semaine'); 
			}
		}

		# la boite infos des auteurs
		if ($objet == 'auteur') {
			// ne pas proposer de créer un rendez-vous pour un auteur à la poubelle ou anonymisée
			if (
				$id 
				&& ($statut = sql_getfetsel('statut', 'spip_auteurs', 'id_auteur=' . $id))
				&& !in_array($statut, ['poubelle','anonyme'])
				&& include_spip('inc/presentation') 
			){
				$texte .=  icone_horizontale(
					_T('erdv:icone_creer_erdv'),
					generer_url_ecrire('erdv_edit', "id_erdv=new&lier_objet=auteur|$id"),
					'erdv-xx.svg',
					'add',
					'center'
				);
			}
		}

		if (
			$texte 
			&& ($p = strpos($flux['data'], '<!--nb_elements-->'))
		){
			$flux['data'] = substr_replace($flux['data'], $texte, $p, 0);
		} else {
			// le commentaire nb_elements n'est pas là, on affiche en fin de texte
			$flux['data'] .= $texte;
		}
	}
	return $flux;
}