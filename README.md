# Des rendez-vous, économes !

Ce plugin permet de créer des rendez-vous et de gérer des calendriers très économes, depuis *l'espace privé*. 

Il propose notamment des mécanismes d'archivage et d'anonymisation automatique afin d'éviter la conservation de données personnelles sur durée inadaptée. Les champs de saisies invitent à une minimisation des données collectées.

Le plugin n'utilise aucune librairie et il est totalement autonome (pas besoin des javascripts de FullCalendar ou d'installer le plugin Agenda).

Documentation complète sur https://contrib.spip.net/Des-rendez-vous-economes

# Objets éditoriaux

Deux objets éditoriaux sont proposés
1. des *rendez-vous* (table `spip_erdvs`). Ces rendez-vous sont rattachés à un calendrier (parent explicite). Ils peuvent être associés à d'autres objets éditioriaux (table auxiliaire avec une colonne rôle `spip_erdvs_liens`).
2. des *calendriers* (table `spip_ecalendriers`) qui peuvent être associés à d'autres objets éditioriaux  (table auxiliaire avec une colonne rôle `spip_ecalendriers_liens`).

# Vues

Plusieurs vues sont proposées
1. la vue du *contenu* des objets rendez-vous ou calendriers créés.
2. Un *planning de la semaine* (exec `planning_semaine`) qui affiche très simplement, sous la forme d'un tableau, les rendez-vous d'un calendrier. Après la description des colonnes dont chacune représente un jour de la semaine, dans un premier bloc sont affichées les rendez-vous d'une durée d'une journée ou de plusieurs journées, puis dans un autre bloc les rendez-vous par heures.

# Nécessite

Le plugin nécessite les plugins suivants :
- Saisies pour Formulaires,
- API Vérifier,
- Objet archiver anonymiser,
- YAML.

# Utilise

Si le [plugin Coordonnées](https://plugins.spip.net/coordonnees.html) est activé et convenablement configuré,  alors vos rendez-vous pourront être géolocalisés. Par défaut, les coordonnées d'un rendez-vous créé seront celles déclarées comme préférées dans le calendrier parent.
Si le plugin [Bcd (Boutons contacts & rendez-vous)](https://plugins.spip.net/bcd.html) est activé, alors vos rendez-vous pourront être ajoutés au calendrier du téléphone portable ou de l'ordinateur de la personne le consultant.
Si le plugin [Ezcheck (Check Factory)](https://plugins.spip.net/bcd.html) est activé, alors vous aurez accès à un puissant panneau de contrôle pour superviser l'archivage et l'anonymisation des rendez-vous mais aussi importer les dates de vacances scolaires ou des jours fériés dans vos calendriers.
Si le plugin [Contacts (Contacts et Organisations)](https://plugins.spip.net/contacts.html) est activé, et que la configuration permet d'associer une organisation à un calendrier, alors non seulement les auteurs associés directement au calendrier pourront voir et modifier les rendez-vous mais également les auteurs qui sont les contacts de l'organisation associée au calendrier.

# Inclusions pour les thèmes

| Fichier                           | Description                                        |
| :-------------------------------- |:---------------------------------------------------|
| `../inclure/erdv_fiche.html`      | affiche le rendez-vous                             |
| `../inclure/erdv_adresses.html`   | affiche l'adresse du rendez-vous                   |
| `../inclure/erdv_documents.html`  | affiche les documents associés au rendez-vous      |
| `../inclure/erdv_icalc.html`      | ajoute un bouton pour récupérer le rendez-vous (icalc) sur un calendrier. Nécessite le plugin Boutons contacts & rendez-vous |

# Personnalisation de l'aspect des rendez-vous dans les calendriers

Des classes peuvent être ajouter à un rendez-vous afin de lui donner le style que vous souhaitez. Les styles proposés sont rassemblés dans un ou plusieurs fichiers YAML placés dans le repertoire `\erdv` à la racine du présent plugin (ou du plugin que vous développerez). Le format de nom à utiliser est `*_class.yaml`. Le fichier `base_class.yaml` propose de réutiliser les styles des Alertes de SPIP et ajoute quelques autres styles. Les nouveaux styles sont définis dans `/prive/style_prive_plugin_erdv.html`. Vous pouvez définir les vôtres de la même manière dans votre plugin.

