Le {statut} du rendez-vous correspond à sa situation. La plupart des personnes connectées à l'espace privé du site ne peuvent pas modifier les statuts des rendez-vous, mais les personnes responsables de la gestion du site, le peuvent. 

| {{Statut}} | {{Puce}} | {{Correspondance}} |
| {{publié}} | <erdv_icone|img=puce-publier-xx.svg> | Un rendez-vous publié est un rendez-vous qui va appraitre dans son calendrier. |
| {{archivé}} | <erdv_icone|img=puce-archiver2-xx.svg> | Un rendez-vous archivé correspond à la première étape du retrait des données individuelles de notre base de données. Le rendez-vous n'est plus visible dans le calendrier. Il reste accessible aux  personnes responsables de la gestion du site (les administratrices ou les administrateurs).|
| {{anonymisé}} | <erdv_icone|img=puce-anonymiser-xx.svg> | Un rendez-vous anonymisé correspond à la seconde étape du retrait des données individuelles de notre base de données. Il s'agit d'une modification des informations du rendez-vous pour les rendre définitivement illisibles. Les données nécessaires aux statistiques seront concervées. |
| {{à la poubelle}} | <erdv_icone|img=puce-supprimer-xx.svg> | Un rendez-vous à la poubelle correspond à un état transitoire vers la destruction définitive de l'ensemble des informations du rendez-vous. C'est à son existance même qu'il sera mis un terme. Aucune donnée ne sera conservée.|


Périodiquement, les rendez-vous sont archivés puis anonymisés de façon à protéger les personnes citées et les informations qu'elles révèlent à la structure. 

| {{Statut}} | {{Délai}} |
| {{archivé}} | <erdv_delai|param=delai_pour_archiver> |
| {{anonymisé}} | <erdv_delai|param=delai_pour_anonymiser> |


