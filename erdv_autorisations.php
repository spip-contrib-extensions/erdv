<?php
/**
 * Fichier gérant les autorisations du plugin
 *
 * @plugin     erdv
 * @copyright  2024
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\erdv\Installation
 */

if (!defined('_ECRIRE_INC_VERSION')) return;
 
// declaration vide pour ce pipeline.
function erdv_autoriser(){}
 
/**
 * Deux objets éditoriaux : 
 * - erdvs
 * - ecalendriers
 */
 
// -----------------
// Objet Rendez-vous (erdv)

// bouton de menu
function autoriser_erdvs_menu_dist($faire, $type, $id, $qui, $opts){
	 return TRUE;
}

// bouton de menu Outils rapides
function autoriser_erdvcreer_menu_dist($faire, $type, $id, $qui, $opt) {
	 return $qui['statut'] == '0minirezo' AND !$qui['restreint'];
}

// iconifier (mettre un logo)
function autoriser_erdv_iconifier_dist($faire, $type, $id, $qui, $opt) {
	// pas de logo sur un rendez-vous : il faut qu'un rendez-vous reste un objet simple
	return FALSE;
}

// creer
function autoriser_erdv_creer_dist($faire, $type, $id, $qui, $opt) {
	// donner l'autorisation de créer, sachant que le formulaire
	// prend le relai pour voir où
	return true;
}
 
// voir
function autoriser_erdv_voir_dist($faire, $type, $id, $qui, $opt) {
	// l'admin peut tout voir
	if ( $qui['statut'] == '0minirezo' && !$qui['restreint']) return TRUE;
	// initialisation
	if (isset($opt['id_ecalendrier'])){
		$id_ecalendrier = (int) $opt['id_ecalendrier'];
	}
	if (!isset($id_ecalendrier) && $id > 0){
		$id_ecalendrier = sql_getfetsel('id_ecalendrier', 'spip_erdvs', 'id_erdv=' . intval($id));
	}
	// vérification de l'autorisation de l'auteur à voir rendez-vous du calendrier
	if (
		isset($id_ecalendrier)
		&& ($id_auteur = (int) $qui['id_auteur'])
		&& ($calendriers = charger_fonction('liste_ecalendriers_auteur', 'inc', true))
	){
		return in_array($id_ecalendrier, $calendriers($id_auteur));
	}
	return FALSE;
}
 
// modifier
function autoriser_erdv_modifier_dist($faire, $type, $id, $qui, $opt) {
	// l'admin peut tout
	if ( $qui['statut'] == '0minirezo' AND !$qui['restreint']) return TRUE;
	// celui qui peut voir peut modifier
	return autoriser('voir', $type, $id, $qui, $opt);
}

// instituer (changer les statuts)
function autoriser_erdv_instituer_dist ($faire, $type, $id, $qui, $opt) {
	// on ne peut aller que d'anonyme à poubelle...
	if (
		$id = (int) $id
		and $precedent = sql_getfetsel('statut', 'spip_erdvs', "id_erdv=$id")
		and $precedent == 'anonyme'
		and  !in_array( $opt['statut'],['poubelle','anonyme'] )
	){
		return false;
	}
	// ... sinon l'admin fait tout...
	if ($qui['statut'] == '0minirezo' and !$qui['restreint']){
		return true;
	}
	// ... et les autres ne peuvent ni archiver ni anonymiser 
	return !in_array($opt['statut'], ['archive', 'anonymise']);
}

/**
 * Définition de l'autorisation d'association d'un auteur à un rendez-vous
 *
 * Il est possible de contourner l'autorisation générale `associerauteur`
 * en définissant une autorisation spécifique sur le `type`
 * sur le format : `autoriser_type_faire[_dist]`
 *
 * @link: https://contrib.spip.net/autorisations-dans-spip
 */
function autoriser_erdv_associerauteurs_dist($faire, $type, $id, $qui, $opt) {
	// l'admin peut tout
	if ( $qui['statut'] == '0minirezo' AND !$qui['restreint']) return true;
	return false;
}
// -----------------
// Objet Calendrier (ecalendrier)

// créer un erdv dans un ecalendrier
function autoriser_ecalendrier_creererdvdans_dist($faire, $type, $id, $qui, $opt) {
	// l'admin peut tout
	if ( $qui['statut'] == '0minirezo' AND !$qui['restreint']) return TRUE;
	// l'auteur associé à un calendrier peut y créer des rendez-vous 
	if ($id = (int) $id){
		return autoriser('voir', 'erdv', 0, $qui, ['id_ecalendrier' => $id]);
	}
	return FALSE; 
}

// bouton de menu
function autoriser_ecalendriers_menu_dist($faire, $type, $id, $qui, $opts){
	 return true;
}

function autoriser_ecalendriersemaine_menu_dist($faire, $type, $id, $qui, $opts){
	 return true;
 }
 
// creer
function autoriser_ecalendrier_creer_dist($faire, $type, $id, $qui, $opt) {
	 return $qui['statut'] == '0minirezo' AND !$qui['restreint']; 
}
 
// voir
function autoriser_ecalendrier_voir_dist($faire, $type, $id, $qui, $opt) {
	if ($qui['statut'] == '0minirezo' && !$qui['restreint']) return true; 
	return autoriser('voir', 'erdv', '0', $qui, ['id_ecalendrier' => $id]);
}
 
// modifier
function autoriser_ecalendrier_modifier_dist($faire, $type, $id, $qui, $opt) {
	 return $qui['statut'] == '0minirezo' AND !$qui['restreint']; 
}
 
 // supprimer
function autoriser_ecalendrier_supprimer_dist($faire, $type, $id, $qui, $opt) {
	return $qui['statut'] == '0minirezo' AND !$qui['restreint']; 
}
 
// associer (lier / delier)
function autoriser_associerecalendrier_dist($faire, $type, $id, $qui, $opt) {
	 return $qui['statut'] == '0minirezo' AND !$qui['restreint']; 
}

 