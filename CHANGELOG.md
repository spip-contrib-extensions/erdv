# Plugin erdv
- Tenir un changelog : https://www.spip.net/fr_article6825.html
- Ecrire un commit : https://www.spip.net/fr_article6824.html

## [Unreleased]

## CHANGED
- #1 `lang/` adopter la syntaxe valide à partir de SPIP 4.1 pour les fichiers de langue afin de préparer la migration en SPIP v5

## FIXED
- #2 Créer un rendez-vous sans qu'il propose par défaut une récurrence.

## ADDED
- #3 Avoir un message en cas de consultation du planning de la semaine alors qu'il n'y a pas de calendrier
- #4 Permettre de supprimer un calendrier
- #5 Utiliser le plugin Contacts et Organisations pour permettre que les auteurs d'une organisation associée à un calendrier puissent voir et modifier les rendez-vous du calendrier.
- #6 Permettre de voir une liste de rendez-vous par calendrier et de procéder à une recherche.
- #7 Permettre de voir la personne organisatrice du rendez-vous
- #8 En cas de calendriers multiples permettre de définir dans les préférences de l'auteur son calendrier par défaut

## REFACTOR
- #9 Nouvelle programmation de l'API en POO. Les fonctions ont été transformées en classes afin de préparer la migration en SPIP v5.

## v1.2.0

## REFACTOR

- `action visualiser_journee` : simplifier encore l'apparence pour simplifier le copier-coller.
- `/inc/erdv.php`. Ajout d'un argument optionnel aux fonctions tetris_journees et tetris_heures
- distinguer la création des données de la présentation des données au travers de deux fichiers
  `/prive/squelettes/contenu/ecalendrier_semaine.html` création du flux
  `/prive/squelettes/inclure/inc-ecalendrier_semaine_tableau.html` affichage du flux

## ADDED

- class `erdv_annonce` : valoriser une annonce importante.
- la personnalisation du regroupement des rendez-vous par type. La colonne type peut accueillir la class que l'on souhaite.
- ajouter autre chose qu'un rendez-vous dans le calendrier. La distinction entre les datas et le layout et l'utilisation de pipeline pour enrichir le flux permet cette possibilité.
- voir un rendez-vous dans son calendrier. Utilisation du pipeline `boite_infos` pour ajouter cette option à la visualisation du contenu d'un rendez-vous.

## v1.1.0 et v 1.1.1

Mise à jour pour indiquer la compatibilité avec spip 4.*

## v1.0.4

Lors de la mise à jour pensez à vider le cache pour que la feuille de style s'actualise (ajout de class, voir ADDED visualisation).

### FIXED

- *première colonne* : appelle erronné de la class (1 à la place de 0) dans `ecalendrier_semaine.html`

### ADDED

- *pipeline* `ecalendrier_semaine` : permettre aux plugins de modifier la liste des rendez-vous avant affichage.
- *visualisation* simplifier de la journée : en cliquant sur le titre de la colonne de la journée en cours, permettre d'avoir une liste des rendez-vous facile à copier-coller. L'objectif est d'aider les utilisatrices ou utilisateurs à annoncer les visiteurs (auprès de l'accueil ou de l'équipe en charge) dans un document tiers (email, traitement de texte...).

## v1.0.3

Lors de la mise à jour pensez à vider le cache pour que la feuille de style s'actualise (added classes).

### ADDED

* gestion de classe par rendez-vous
* gestion d'un dashboard si ezcheck est actif
  - gestion des rendez-vous archivés
  - gestion des rendez-vous anonymisés
  - import des vacances scolaires par zone A, B ou C
  - import des jours fériés par zone métropole, Wallis et Futuna...