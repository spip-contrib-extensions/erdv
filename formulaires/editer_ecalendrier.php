<?php 

/**
 * Formulaire pour créer ou modifier un calendrier (ecalendrier)
 */
if (!defined('_ECRIRE_INC_VERSION')) return;
 
 include_spip('inc/actions');
 include_spip('inc/editer');
      
/**
 * Déclaration des champs du formulaire
 *
 * Le formulaire doit permettre la saisie d'un rendez-vous
 * le plus rapidement possible et avec le moins possible de
 * renseignement à donner (fieldset_essentiel).
 * Néanmoins, le formulaire doit aussi permettre 
 * de renseigner des élements plus spécifiques 
 * si nécessaire (fieldset_precisions).
 * Enfin, le formulaire doit prendre en compte
 * une association du calendrier à un objet (fieldset_association).
 *
 * Références des vérifications :
 * @link: https://contrib.spip.net/References-des-verifications
 */
function formulaires_editer_ecalendrier_saisies_dist($id_ecalendrier='new', $retour='', $associer_objet = '', $lier_trad=0, $config_fonc='', $row=[], $hidden=''){
	$id_ecalendrier = (int) $id_ecalendrier;

	$saisies = [
			[
				'saisie' => 'hidden',
				'options' => [
					'nom' => 'id_ecalendrier',
					'defaut' => $id_ecalendrier,
				],
			],
			[
				'saisie' => 'input',
				'options' => [
					'nom' => 'titre',
					'label' => '<:ecalendrier:info_titre:>',
				],
			],
			'verifier' => [ 
				'type' => 'taille',
				'options' => [
					'maxi' => '255',
					'min' => '0',
				],
			],
			[
				'saisie' => 'input',
				'options' => [
					'nom' => 'abreviation',
					'label' => '<:ecalendrier:info_abreviation:>',
					'explication' => '<:ecalendrier:explication_abreviation:>',
				],
			],
		];

	if ($associer_objet){
		include_spip('inc/filtres'); // fonctions pour les objets (necessaire en cas d'association)
		$saisies = array_merge($saisies, [[ // le fieldset d'association
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'fieldset_association',
				'label' => '<:ecalendrier:fieldset_association:>',
				#'icone' => $icone_objet,
			],
			'saisies' => [ // les champs dans le fieldset 
				[ 
					'saisie' => 'case',
					'options' => [
						'nom' => 'c_associe',
						'label' => _T('ecalendrier:texte_associer', 
							[ 
								'str' => _T(strstr($associer_objet,'|',true) . ':titre_' . strstr($associer_objet,'|',true)), 
								'nb' => substr(strstr(_request('associer_objet'),'|'),1)
							]
						),
						'valeur_oui' => 'oui', 
						'valeur_non' => 'non',
						'defaut' => 'oui',
					]
				],
			]
		]]);
	}

	return $saisies;
}

/**
  * Identifier le formulaire en faisant abstraction des parametres qui ne representent pas l'objet edite
  */
function formulaires_editer_ecalendrier_identifier_dist($id_ecalendrier='new', $retour='', $associer_objet = '', $lier_trad=0, $config_fonc='', $row=[], $hidden=''){
	return serialize([intval($id_ecalendrier), $associer_objet]);
}
 
/**
  * Declarer les champs postes et y integrer les valeurs par defaut
  */
function formulaires_editer_ecalendrier_charger_dist($id_ecalendrier='new', $retour='', $associer_objet = '', $lier_trad=0, $config_fonc='', $row=[], $hidden=''){
	// Initialisation des valeurs à transmettre au formulaire
	$valeurs = [];

	$valeurs = formulaires_editer_objet_charger('ecalendrier',$id_ecalendrier,'',$lier_trad,$retour,$config_fonc,$row,$hidden);

	# Une association 
	$valeurs['associer_objet'] = $associer_objet ? $associer_objet : _request('associer_objet');
	if ($valeurs['associer_objet']) {
		$valeurs['c_associer'] = 'oui';
	}

	return $valeurs;
}


/**
  * Traiter les champs postes
  */
function formulaires_editer_ecalendrier_traiter_dist($id_ecalendrier='new', $retour='', $associer_objet = '', $lier_trad=0, $config_fonc='', $row=[], $hidden=''){

	// rompre l'association si nécessaire
	if (_request('c_associe') == 'non'){
		$associer_objet = '';
	}

	$retours = formulaires_editer_objet_traiter('ecalendrier', $id_ecalendrier, '', $lier_trad, $retour,$config_fonc, $row, $hidden);

	// Un lien a prendre en compte ?
	if ($associer_objet and $id_ecalendrier = $retours['id_ecalendrier']) {
		list($objet, $id_objet) = explode('|', $associer_objet);
		if ($objet and $id_objet == intval($id_objet)) {
			include_spip('action/editer_liens');
			objet_associer(
				['calendrier' => $id_ecalendrier],
				[$objet => $id_objet]
			);
			// rediriger vers l'objet
			if (isset($redirect)) {
				$retours['redirect'] = parametre_url($redirect, 'id_lien_ajoute', $id_ecalendrier, '&');
			}
		}
	}
	return $retours;
}