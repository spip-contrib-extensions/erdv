<?php

/*
 * Formulaire de configuration du plugin
 *
 * Documentation sur le plugin Saisies pour formulaires et la configuration :
 * link : https://contrib.spip.net/Formulaire-de-configuration-avec-le-plugin-Saisies
 */

//Sécurité
if (!defined('_ECRIRE_INC_VERSION')) return;
 
 
/**
 * Un simple formulaire de config
 *
 * Le plugin propose :
 * - le délai avant archivage des rendez-vous
 * - le délai avant anonymisation des rendez-vous archivés
 * - de placer par défaut le lieu déclarer dans le calendrier comme lieu de survenance du rendez-vous
 *
 * @link : https://contrib.spip.net/Formulaire-de-configuration-avec-le-plugin-Saisies
 **/
function formulaires_configurer_erdv_saisies_dist(){

	$saisies = [
		[
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'ecologie',
				'label' => _T('erdv:config_fieldset_ecologie'),
				'explication' => '<:erdv:config_explication_ecologie:>',
			],
			'saisies' => [ 
				[
					'saisie' => 'selection',
					'options' => [
						'nom' => 'delai_pour_archiver',
						'label' => '<:erdv:config_label_delai_pour_archiver:>',
						'explication' => '<:erdv:config_explication_delai_pour_archiver:>',
						'cacher_option_intro' => 'oui',
						'defaut' => '+1 month',
						'data' => [
							'-1 week' => '<:erdv:config_data_1_semaine:>',
							'-2 week' => '<:erdv:config_data_2_semaine:>',
							'-1 month' => '<:erdv:config_data_1_mois:>',
							'-2 month' => '<:erdv:config_data_2_mois:>'
						]
					]
				],
				[
					'saisie' => 'selection',
					'options' => [
						'nom' => 'delai_pour_anonymiser',
						'label' => '<:erdv:config_label_delai_pour_anonymiser:>',
						'explication' => '<:erdv:config_explication_delai_pour_anonymiser:>',
						'cacher_option_intro' => 'oui',
						'defaut' => '+3 month',
						'data' => [
							'-2 week' => '<:erdv:config_data_2_semaine:>',
							'-1 month' => '<:erdv:config_data_1_mois:>',
							'-2 month' => '<:erdv:config_data_2_mois:>',
							'-3 month' => '<:erdv:config_data_3_mois:>',
							'-4 month' => '<:erdv:config_data_4_mois:>'
						]
					]
				],
				[
					'saisie' => 'selection',
					'options' => [
						'nom' => 'duree_du_rdv',
						'label' => '<:erdv:config_label_duree_du_rdv:>',
						'explication' => '<:erdv:config_explication_duree_du_rdv:>',
						'cacher_option_intro' => 'oui',
						'defaut' => '+1 hour',
						'data' => [
							'+15 minutes' => '<:erdv:config_data_15_minutes:>',
							'+20 minutes' => '<:erdv:config_data_20_minutes:>',
							'+30 minutes' => '<:erdv:config_data_30_minutes:>',
							'+45 minutes' => '<:erdv:config_data_45_minutes:>',
							'+1 hour' => '<:erdv:config_data_1_heure:>'
						]
					]
				],
				[
					'saisie' => 'case',
					'options' => [
						'nom' => 'lieu_du_calendrier',
						'label' => '<:erdv:config_label_lieu_du_calendrier:>',
						'label_case' => '<:erdv:config_label_case_lieu_du_calendrier:>',
						'explication' => '<:erdv:config_explication_lieu_du_calendrier:>',
						'conteneur_class' => 'pleine_largeur',
						'valeur_oui' => 'oui',
						'valeur_non' => 'non',
						'defaut' => 'oui',
					],
				],
			]
		]
	];

	return $saisies;
}

function formulaires_configurer_erdv_verifier_dist() {
	$retours = [];
 
	// vérifier que le délai d’archivage survient avant le délai d’anonymisation
	$delai_pour_archiver = _request('delai_pour_archiver');
	$delai_pour_anonymiser = _request('delai_pour_anonymiser');
	if ( strtotime($delai_pour_archiver) < strtotime($delai_pour_anonymiser) ){
		$retours['message_erreur'] = _T('erdv:config_delai_conflit');
		$retours['editable'] = true;
	} 
 
	return $retours;
}