<?php 

/**
 * Formulaire pour définir son calendrier par défaut
 */
if (!defined('_ECRIRE_INC_VERSION')) return;
 

/**
 * Déclaration des champs du formulaire
 *
 */
function formulaires_editer_ecalendrier_pref_saisies_dist(){
	// avoir la liste des calendriers auxquels à accès l'auteur
	$data =[];
	if ($calendriers = charger_fonction('liste_ecalendriers_auteur','inc', true)) {
		$ecalendriers = $calendriers($GLOBALS['visiteur_session']['id_auteur']);
		if ($liens = sql_allfetsel('id_ecalendrier, titre', 'spip_ecalendriers', sql_in('id_ecalendrier',$ecalendriers))) {
			foreach ($liens as $l) {
				$data[$l['id_ecalendrier']] = $l['titre'];
			}
		}
	}
	$saisies = [
			[
				'saisie' => 'ecalendriers',
				'options' => [
					'nom' => 'defaut_calendrier',
					'label' => '<:ecalendrier:texte_ecalendrier_par_defaut:>',
					'cacher_option_intro' => 'oui',
					'data' => $data,
				],
			],
		];
	return $saisies;
}

/**
  * Declarer les champs postes et y integrer les valeurs par defaut
  */
function formulaires_editer_ecalendrier_pref_charger_dist(){
	// Initialisation des valeurs à transmettre au formulaire
	$valeurs = [];

	// travailler sur des meta fraiches
	include_spip('inc/meta');
	lire_metas();

	// Y-a-t-il déja une préférence définie ?
	if (isset($GLOBALS['visiteur_session']['prefs']['defaut_calendrier'])){
		$valeurs['defaut_calendrier'] = $GLOBALS['visiteur_session']['prefs']['defaut_calendrier'];
	} else {
		// avoir la liste des ecalendriers accessibles
		if ($calendriers = charger_fonction('liste_ecalendriers_auteur','inc', true)) {
			$ecalendriers = $calendriers($GLOBALS['visiteur_session']['id_auteur']);
			if (isset($ecalendriers[0])){
				$valeurs['defaut_calendrier'] = $ecalendriers[0];
			}
		}
	}

	return $valeurs;
}

/**
  * Traiter les champs postes
  */
function formulaires_editer_ecalendrier_pref_traiter_dist(){
	if ($defaut_calendrier = _request('defaut_calendrier')) {
		$GLOBALS['visiteur_session']['prefs']['defaut_calendrier'] = $defaut_calendrier;
	}

	if (intval($GLOBALS['visiteur_session']['id_auteur'])) {
		include_spip('action/editer_auteur');
		$c = ['prefs' => serialize($GLOBALS['visiteur_session']['prefs'])];
		auteur_modifier($GLOBALS['visiteur_session']['id_auteur'], $c);
	}

	return ['message_ok' => _T('config_info_enregistree'), 'editable' => true];
}