<?php 

/**
 * Formulaire pour créer ou modifier un rendez-vous (erdv)
 */
if (!defined('_ECRIRE_INC_VERSION')) return;
 
include_spip('inc/actions');
include_spip('inc/editer');
include_spip('inc/filtres'); // fonctions pour les objets (necessaire en cas d'association)

/**
 * Déclaration des champs du formulaire
 *
 * Le formulaire doit permettre la saisie d'un rendez-vous
 * le plus rapidement possible et avec le moins possible de
 * renseignement à donner (fieldset_essentiel).
 * Néanmoins, le formulaire doit aussi permettre 
 * de renseigner des élements plus spécifiques 
 * si nécessaire (fieldset_precisions).
 * Enfin, le formulaire doit prendre en compte
 * une association du rdv à un objet (fieldset_association),
 * ou une liaison de l'objet au rdv (fieldset_liaison)
 *
 * Références des vérifications :
 * @link: https://contrib.spip.net/References-des-verifications
 */
function formulaires_editer_erdv_saisies_dist($id_erdv='new', $retour='', $associer_objet = '', $lier_objet=''){
	$id_erdv = (int) $id_erdv;

	$saisies[] = [
		'saisie' => 'fieldset',
		'options' => [
			'nom' => 'fieldset_essentiel',
			'label' => '<:erdv:fieldset_essentiel:>',
		],
		'saisies' => [
			[
				'saisie' => 'hidden',
				'options' => [
					'nom' => 'id_erdv',
					'defaut' => $id_erdv,
				],
			],
			[
				'saisie' => 'input',
				'options' => [
					'nom' => 'titre',
					'label' => '<:erdv:info_titre:>',
					'explication' => '<:erdv:explication_titre:>',
					'obligatoire' => 'oui',
				],
			],
			'verifier' => [
				'type' => 'taille',
				'options' => [
					'maxi' => '255',
					'min' => '1',
				],
			],
			[
				'saisie' => 'textarea',
				'options' => [
					'nom' => 'description',
					'label' => '<:erdv:info_description:>',
					'explication' => '<:erdv:explication_description:>',
					'inserer_barre' => 'edition',
				],
			],
			[
				'saisie' => 'date',
				'options' => [
					'nom' => 'date_debut',
					'label' => '<:erdv:info_date_debut:>',
					'horaire' => 'oui',
					'explication' => '<:erdv:explication_date_debut:>',
				],
				'verifier' => [
					'type' => 'date',
					'options' => [
						'normaliser' => 'datetime',
					],
				],
			],
		],
	];

	$saisies[] = [
		'saisie' => 'fieldset',
		'options' => [
			'nom' => 'fieldset_precisions',
			'label' => '<:erdv:fieldset_precisions:>',
			'explication' => '<:erdv:explication_precisions:>',
			//'pliable' => 'oui',
			//'plie' => 'oui',
		],
		'saisies' => [
			[
				'saisie' => 'ecalendriers',
				'options' => [
					'nom' => 'id_ecalendrier',
					'label' => '<:ecalendrier:titre_ecalendrier:>',
					'cacher_option_intro' => 'oui',
				],
			],
			[
				'saisie' => 'date',
				'options' => [
					'nom' => 'date_fin',
					'label' => '<:erdv:info_date_fin:>',
					'horaire' => 'oui',
				],
				'verifier' => [
					'type' => 'date',
					'options' => [
						'normaliser' => 'datetime',
					],
				],
			],
			[
				'saisie' => 'case',
				'options' => [
					'nom' => 'toute_la_journee',
					'label' => '<:erdv:info_toute_la_journee:>',
					'defaut' => 'non',
					'label_oui' => 'oui',
					'valeur_oui' => 'oui',
					'label_non' => 'non',
					'valeur_non' => 'non',
				],
			],
			[
				'saisie' => 'selection',
				'options' => [
					'nom' => 'recurrence_jour',
					'label' => '<:erdv:info_recurrence_jour:>',
					'cacher_option_intro' => 'oui',
					'data' => [
						0 => '<:erdv:texte_recurrence_jour_non:>',
						1 => '<:date_jour_2:>',
						2 => '<:date_jour_3:>',
						3 => '<:date_jour_4:>',
						4 => '<:date_jour_5:>',
						5 => '<:date_jour_6:>',
						6 => '<:date_jour_7:>',
						7 => '<:date_jour_1:>',
					],
				],
			],
			[
				'saisie' => 'selection',
				'options' => [
					'nom' => 'recurrence_semaine',
					'afficher_si' => "@recurrence_jour@!='0'",
					'label' => '<:erdv:info_recurrence_semaine:>',
					'data' => [
						0 => '<:erdv:date_0_semaine:>',
						1 => '<:erdv:date_1_semaine:>',
						2 => '<:erdv:date_2_semaine:>',
						3 => '<:erdv:date_3_semaine:>',
						4 => '<:erdv:date_4_semaine:>',
						5 => '<:erdv:date_5_semaine:>',
						6 => '<:erdv:date_6_semaine:>',
						7 => '<:erdv:date_7_semaine:>',
						8 => '<:erdv:date_8_semaine:>',
						9 => '<:erdv:date_9_semaine:>',
					],
					'defaut' => 1,
					'cacher_option_intro' => 'oui',
				],
			],
			[
				'saisie' => 'erdv_class',
				'options' => [
					'nom' => 'class',
					'label' => '<:erdv:info_class:>',
					'defaut' => 'non',
				],
			],
		],
	];

	if ($associer_objet){
		$saisies[] = [ // le fieldset d'association
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'fieldset_association',
				'label' => '<:erdv:fieldset_association:>',
				#'icone' => $icone_objet,
			],
			'saisies' => [ // les champs dans le fieldset 
				[ 
					'saisie' => 'case',
					'options' => [
						'nom' => 'c_associe',
						'label' => _T('erdv:texte_associer_a', 
							[ 
								'str' => _T(strstr($associer_objet,'|',true) . ':titre_' . strstr($associer_objet,'|',true)), 
								'nb' => substr(strstr($associer_objet, '|'), 1)
							]
						),
						'valeur_oui' => 'oui', 
						'valeur_non' => 'non',
						'defaut' => 'oui',
					]
				]
			]
		];
	}
	if ($lier_objet){
		$saisies[] = [ // le fieldset de liaison
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'fieldset_liaison',
				'label' => '<:erdv:fieldset_liaison:>',
				#'icone' => $icone_objet,
			],
			'saisies' => [ // les champs dans le fieldset 
				[ 
					'saisie' => 'case',
					'options' => [
						'nom' => 'c_lie',
						'label' => _T('erdv:texte_lier_a', 
							[ 
								'str' => _T(strstr($lier_objet,'|',true) . ':titre_' . strstr($lier_objet,'|',true)), 
								'nb' => substr(strstr($lier_objet, '|'), 1)
							]
						),
						'valeur_oui' => 'oui', 
						'valeur_non' => 'non',
						'defaut' => 'oui',
					]
				]
			]
		];
	}
	return $saisies;
}

/**
 * Identifier le formulaire en faisant abstraction des paramètres qui ne représentent pas l'objet edité.
 *
 * @param int|string $id_erdv  Identifiant unique. 'new' pour un nouveau rendez-vous.
 * @param string     $retour         URL de redirection après le traitement
 * @param string     $associer_objet Éventuel `objet|x` indiquant d'associer le rendez-vous créé à cet objet,
 *                                   tel que `article|3`
 * @param string     $lier_objet     Éventuel `objet|x` indiquant de lier l'objet au rendez-vous créé,
                                     (ou d'associer l'objet au rendez-vous créé)
 *                                   tel que `auteur|1`
 *
 * @return string Hash du formulaire
 */
function formulaires_editer_erdv_identifier_dist($id_erdv='new', $retour='', $associer_objet = '', $lier_objet=''){
	return serialize([(int) $id_erdv, $associer_objet, $lier_objet]);
}
 
/**
 * Declarer les champs postes et y integrer les valeurs par defaut
 *
 * @param int|string $id_erdv  Identifiant unique. 'new' pour un nouveau rendez-vous.
 * @param string     $retour         URL de redirection après le traitement
 * @param string     $associer_objet Éventuel `objet|x` indiquant d'associer le rendez-vous créé à cet objet,
 *                                   tel que `article|3`
 * @param string     $lier_objet     Éventuel `objet|x` indiquant de lier l'objet au rendez-vous créé,
                                     (ou d'associer l'objet au rendez-vous créé)
 *                                   tel que `auteur|1`
 *
 * @return array Les valeurs par défaut
 */
function formulaires_editer_erdv_charger_dist($id_erdv='new', $retour='', $associer_objet = '', $lier_objet=''){
	// Initialisation des valeurs à transmettre au formulaire
	$valeurs = [];

	$valeurs = formulaires_editer_objet_charger('erdv',$id_erdv,'',0,$retour,'',[],'');

	# une création
	if (! $id_erdv = (int) $valeurs['id_erdv']){

		// est-ce qu'un calendrier est suggéré ?
		if ($id_ecalendrier = (int) _request('id_ecalendrier')
			and include_spip('inc/autoriser')
		){
			if (autoriser('creererdvdans', 'ecalendrier', $id_ecalendrier)) {
				$valeurs['id_ecalendrier'] = $id_ecalendrier;
			} else {
				return ['message_erreur' => _T('erdv:erreur_1_calendrier'), 'editable' => FALSE];
			}
		} else {
			if (!autoriser('creererdvdans', 'ecalendrier', 0)) {
				return ['message_erreur' => _T('erdv:erreur_aucun_calendrier'), 'editable' => FALSE];
			}
		}

		// est-ce qu'un titre est suggéré ?
		if ($titre = (string) _request('titre')
			and $titre = trim($titre)
		){
			$valeurs['titre'] = $titre;
		}
		// est-ce qu'une date et heure est proposée ?
		if ($time_debut = (int) _request('time_debut')){
			$valeurs['date_debut'] = date('Y-m-d H:i:s', $time_debut);
			//est-ce qu'il y a une date de fin également proposée ?
			if ($time_fin = (int) _request('time_fin')){
				$valeurs['time_fin'] = date('Y-m-d H:i:s', $time_fin);
			}
		} else {
			// renseigner l'heure de début
			$minutes = 30;
			$heure = date('H:i:s');
			$secondes = strtotime($heure);
			$arrondir = round($secondes / ($minutes * 60)) * ($minutes * 60);
			$heure = date('H:i:s', $arrondir);
			$valeurs['date_debut'] = date('Y-m-d') . $heure;
		}
		// est-ce qu'un rendez-vous pour la journée entière est proposée ?
		if (
			($toute_la_journee = (string) _request('toute_la_journee'))
			&& $toute_la_journee === 'oui'
		){
			$valeurs['toute_la_journee'] = 'oui';
		}
		// est-ce qu'une récurrence de jour est proposée ?
		if (
			($recurrence_jour = (int) _request('recurrence_jour'))
			&& $recurrence_jour > 0
			&& $recurrence_jour < 8
		){
			$valeurs['recurrence_jour'] = $recurrence_jour;
		}
		// est-ce qu'une récurrence de la semaine est proposée ?
		if (
			($recurrence_semaine = (int) _request('recurrence_semaine'))
			&& $recurrence_semaine > -1
			&& $recurrence_semaine < 10
		){
			$valeurs['recurrence_semaine'] = $recurrence_semaine;
			// en l'absence de valeur, donner par défaut 
			// une récurrence de jour sur le jour actuel de la semaine
			if (!isset($valeurs['recurrence_jour']) || $valeurs['recurrence_jour'] == 0){
				$valeurs['recurrence_jour'] = date('N', strtotime($valeurs['date_debut']));
			}
		}
	}

	# Une association 
	$valeurs['associer_objet'] = $associer_objet ? $associer_objet : _request('associer_objet');
	if ($valeurs['associer_objet']) {
		$valeurs['c_associer'] = 'oui';
	}
	# Une liaison 
	$valeurs['lier_objet'] = $lier_objet ? $lier_objet : _request('lier_objet');
	if ($valeurs['lier_objet']) {
		$valeurs['c_lier'] = 'oui';
	}

	return $valeurs;
}


/**
 * Traiter les champs postes
 *
 * @uses formulaires_editer_objet_traiter, config, editer_liens
 *
 * @param int|string $id_erdv  Identifiant unique. 'new' pour un nouveau rendez-vous.
 * @param string     $retour         URL de redirection après le traitement
 * @param string     $associer_objet Éventuel `objet|x` indiquant d'associer le rendez-vous créé à cet objet,
 *                                   tel que `article|3`
 * @param string     $lier_objet     Éventuel `objet|x` indiquant de lier l'objet au rendez-vous créé,
                                     (ou d'associer l'objet au rendez-vous créé)
 *                                   tel que `auteur|1`
 *
 * @return array Retour des traitements
 */
function formulaires_editer_erdv_traiter_dist($id_erdv='new', $retour='', $associer_objet = '', $lier_objet=''){

	// rompre l'association si nécessaire
	if (_request('c_associe') == 'non'){
		$associer_objet = '';
	}
	// rompre la liaison si nécessaire
	if (_request('c_lie') == 'non'){
		$lier_objet = '';
	}
	$date_debut = _request('date_debut');
	// a-t-on une date_fin ?
	if ($date_fin = _request('date_fin') and $date_fin != '0000-00-00 00:00:00'){
		if ($date_debut > $date_fin){
			return ['message_erreur' => _T('erdv:erreur_ordre_dates')];
		}
	} else {
		// S’il n'y a pas d’heure de fin, on la place par défaut
		include_spip('inc/config');
		$duree = lire_config('erdv/duree_du_rdv', '+1 hour');
		$date_fin = date('Y-m-d H:i:s', strtotime($date_debut . ' ' . $duree));
		set_request('date_fin', $date_fin);
	}
	// si on a un jour récurrent, la date_debut et la date_fin doit être sur ce jour.
	if ( $recurrence_jour = _request('recurrence_jour') ){
		if( date('N',strtotime($date_debut)) != $recurrence_jour
			or 
			date('N',strtotime($date_fin)) != $recurrence_jour
		){
			return ['message_erreur' => _T('erdv:erreur_recurrencejour_dates')];
		}
	}

	$retours = formulaires_editer_objet_traiter('erdv', $id_erdv, '', '', $retour, '', [], '');

	// Un objet est à associer au rendez-vous ?
	if (
		$lier_objet
		&& ($id_erdv = $retours['id_erdv'])
	){
		list($objet, $id_objet) = explode('|', $lier_objet);
		if ($objet && ($id_objet = (int) $id_objet)) {
			include_spip('action/editer_liens');
			objet_associer(
				[$objet => $id_objet],
				['erdv' => $id_erdv]
			);
			// rediriger vers l'objet
			$retours['redirect'] = generer_url_ecrire('erdv', 'id_erdv='.$id_erdv);
		}
	}
	// Le rendez-vous est-il à associer à un objet ?
	if ($associer_objet
		&& ($id_erdv = $retours['id_erdv'])
	){
		list($objet, $id_objet) = explode('|', $associer_objet);
		if ($objet and $id_objet == intval($id_objet)) {
			include_spip('action/editer_liens');
			objet_associer(
				['erdv' => $id_erdv],
				[$objet => $id_objet]
			);
			// rediriger vers l'objet
			if (isset($redirect)) {
				$retours['redirect'] = parametre_url($redirect, 'id_lien_ajoute', $id_erdv, '&');
			}
		}
	}

	return $retours;
}