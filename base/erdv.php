<?php
/**
 * Déclarations relatives à la base de données
 *
 * @plugin     erdv
 * @copyright  2024
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Erdv\Base
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Déclaration des alias de tables et filtres automatiques de champs
 *
 * Le plugin
 * - déclare les alias de tables des calendriers et des rendez-vous
 * - déclare un traitement pour la description des rendez-vous
 *   @link : https://programmer.spip.net/Traitements-automatiques-des
 *
 * @pipeline declarer_tables_interfaces
 *
 * @param array $interfaces
 *     Déclarations d'interface pour le compilateur
 * @return array
 *     Déclarations d'interface pour le compilateur
 */
function erdv_declarer_tables_interfaces($interfaces) {

	$interfaces['table_des_tables']['erdvs'] = 'erdvs';
	$interfaces['table_des_tables']['ecalendriers'] = 'ecalendriers';
	$interfaces['table_des_traitements']['DESCRIPTION']['erdvs'] = _TRAITEMENT_RACCOURCIS;
	
	return $interfaces;
}


/**
 * Déclaration des objets éditoriaux
 *
 * Création de deux objets :
 * - des rendez-vous
 * Une instanciation limitée à publie|archive|anonyme|poubelle.
 * @link https://contrib.spip.net/Objet-archiver-anonymiser
 * Pas de gestion de version,
 * ni de gestion de multilinguisme.
 * La création doit restée simple et économe en énergie.
 * - des calendriers
 * Chaque rdv a pour parent explicite un calendrier.
 *
 * @pipeline declarer_tables_objets_sql
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function erdv_declarer_tables_objets_sql($tables) {

	$tables['spip_erdvs'] = [
		'type' => 'erdv',
		'table_objet' => 'erdvs',
		'table_objet_surnoms' => [],
		'principale' => 'oui',
		'field' => [
			'id_erdv' => 'bigint(21) NOT NULL',
			'id_ecalendrier' => 'bigint(21) DEFAULT "0" NOT NULL',
			'date_debut' => 'datetime NOT NULL DEFAULT "0000-00-00 00:00:00"',
			'date_fin' => 'datetime NOT NULL DEFAULT "0000-00-00 00:00:00"',
			'toute_la_journee' => 'varchar(3) DEFAULT "non" NOT NULL',
			'recurrence_jour' => 'varchar(1) DEFAULT "0" NOT NULL',
			'recurrence_semaine' => 'varchar(1) DEFAULT "0" NOT NULL',
			'titre' => 'tinytext NOT NULL DEFAULT ""',
			'description' => 'text NOT NULL DEFAULT ""',
			'class' => 'varchar(255) NOT NULL DEFAULT "non"',
			'statut' => 'varchar(10) DEFAULT "0" NOT NULL',
			'maj' => 'TIMESTAMP',
		],
		'key' => [
			'PRIMARY KEY' => 'id_erdv',
			'KEY id_ecalendrier' => 'id_ecalendrier',
			'KEY date_debut' => 'date_debut',
			'KEY date_fin' => 'date_fin',
			'KEY toute_la_journee' => 'toute_la_journee',
			'KEY recurrence_jour' => 'recurrence_jour',
			'KEY recurrence_semaine' => 'recurrence_semaine',
			'KEY statut' => 'statut',
		],
		'champs_contenu' => [
			'id_ecalendrier',
			'date_debut',
			'date_fin', 
			'toute_la_journee',
			'recurrence_jour',
			'recurrence_semaine',
			#'titre', // titre
			'description',
			#'class'
		],
		'champs_editables'  => [
			'id_ecalendrier',
			'date_debut',
			'date_fin',
			'toute_la_journee',
			'recurrence_jour',
			'recurrence_semaine',
			'titre',
			'description',
			'class'
		],
		'champs_anonymises' => [
			'titre' => 'void', // rien
			'description' => 'void', // rien
		],
		'parent' => [
			[
				'type' => 'ecalendrier',
				'champ' => 'id_ecalendrier'
			]
		],
		'titre' => 'titre AS titre',
		#'date' => '',  // indique le nom du field pour le formulaires_dater_charger_dist
		'rechercher_champs' => [
			'titre' => 8,
			'description' => 3
		],
		'statut' => [
			[
				'champ' => 'statut',
				'publie' => 'publie',
				'previsu' => '',
				'post_date' => '',
				'exception' => ['statut', 'tout']
			]
		],
		'statut_titres' => [
			'publie' => 'erdv:info_erdv_publie',
			'archive' => 'erdv:info_erdv_archive',
			'anonyme' => 'objet_archiver_anonymiser:info_anonymise',
			'poubelle' => 'objet_archiver_anonymiser:info_archive'
		],
		'statut_textes_instituer' => [
			'publie' => 'texte_statut_publie',
			'archive' => 'objet_archiver_anonymiser:statut_archive',
			'anonyme' => 'objet_archiver_anonymiser:statut_anonymise',
			'poubelle' => 'texte_statut_poubelle',
		],
		'statut_images' => [
			'publie' => 'puce-publier-xx.svg',
			'archive' => 'puce-archiver2-xx.svg',
			'anonyme' => 'puce-anonymiser-xx.svg',
			'poubelle' => 'puce-supprimer-xx.svg',
		],
		'texte_changer_statut' => 'erdv:texte_changer_statut',
		'aide_changer_statut' => 'erdv/statut',
		'tables_jointures'  => [
			'spip_rdvs_liens'
		],
		'texte_retour'       => 'icone_retour',
		'texte_objets'       => 'erdv:titre_erdvs',
		'texte_objet'        => 'erdv:titre_erdv',
		'texte_modifier'     => 'erdv:icone_modifier_erdv',
		'texte_creer'        => 'erdv:icone_creer_erdv',
		'info_aucun_objet'   => 'erdv:info_aucun_erdv',
		'info_1_objet'       => 'erdv:info_1_erdv',
		'info_nb_objets'     => 'erdv:info_nb_erdvs',
		'texte_logo_objet'   => 'erdv:titre_logo_erdv',

		// prévoit une gestion des rôles mais n'en fournit pas
		'roles_colonne' => 'role',
		'roles_titres'  => [],
		'roles_objets'  => [],
	];

	$tables['spip_ecalendriers'] = [
		'type' => 'ecalendrier',
		'table_objet' => 'ecalendriers',
		'table_objet_surnoms' => [],
		'principale' => 'oui',
		'field' => [
			'id_ecalendrier' => 'bigint(21) NOT NULL',
			'titre' => 'tinytext NOT NULL DEFAULT ""',
			'abreviation' => 'tinytext NOT NULL DEFAULT ""',
			'maj' => 'TIMESTAMP',
		],
		'key' => [
			'PRIMARY KEY' => 'id_ecalendrier',
		],
		'champs_contenu' => [
			#'titre',
			'abreviation', 
		],
		'champs_editables'  => [
			'titre',
			'abreviation',
		],
		'rechercher_champs' => ['titre' => 8, 'abreviation' => 3],
		'tables_jointures'  => [
			'ecalendriers_liens',
			'id_auteur' => 'auteurs_liens',
			'id_organisation' => 'organisations_liens'
		],
		'titre' => 'titre AS titre',
		#'date' => 'date_debut',  // indique le nom du field pour le formulaires_dater_charger_dist
		'texte_retour'       => 'icone_retour',
		'texte_objets'       => 'ecalendrier:titre_ecalendriers',
		'texte_objet'        => 'ecalendrier:titre_ecalendrier',
		'texte_modifier'     => 'ecalendrier:icone_modifier_ecalendrier',
		'texte_creer'        => 'ecalendrier:icone_creer_ecalendrier',
		'info_aucun_objet'   => 'ecalendrier:info_aucun_ecalendrier',
		'info_1_objet'       => 'ecalendrier:info_1_ecalendrier',
		'info_nb_objets'     => 'ecalendrier:info_nb_ecalendriers',
		'texte_logo_objet'   => 'ecalendrier:titre_logo_ecalendrier',

		// prévoit une gestion des rôles mais n'en fournit pas
		'roles_colonne' => 'role',
		'roles_titres'  => [],
		'roles_objets'  => []
		];

	return $tables;
}

/**
 * Déclaration des tables auxiliaires
 *
 * Généralement les tables auxiliaires sont des tables de laisons
 * permettant d'associer des objets à d'autres objets.
 *
 */
function erdv_declarer_tables_auxiliaires($tables) {

	$tables['spip_erdvs_liens'] = [
		'field' => [
			'id_erdv' => "bigint(21) NOT NULL DEFAULT '0'",
			'id_objet' => "bigint(21) DEFAULT '0' NOT NULL",
			'objet' => "varchar(25) DEFAULT '' NOT NULL",
			'role' => "varchar(30) NOT NULL DEFAULT ''",
		],
		'key' => [
			'PRIMARY KEY' => 'id_erdv,id_objet,objet,role',
			'KEY id_erdv' => 'id_erdv',
			'KEY id_objet' => 'id_objet',
			'KEY objet' => 'objet',
			'KEY role' => 'role',
		]
	];

	$tables['spip_ecalendriers_liens'] = [
		'field' => [
			'id_ecalendrier' => "bigint(21) NOT NULL DEFAULT '0'",
			'id_objet' => "bigint(21) DEFAULT '0' NOT NULL",
			'objet' => "varchar(25) DEFAULT '' NOT NULL",
			'role' => "varchar(30) NOT NULL DEFAULT ''",
		],
		'key' => [
			'PRIMARY KEY' => 'id_ecalendrier,id_objet,objet,role',
			'KEY id_ecalendrier' => 'id_ecalendrier',
			'KEY id_objet' => 'id_objet',
			'KEY objet' => 'objet',
			'KEY role' => 'role',
		],
	];

	return $tables;
}
