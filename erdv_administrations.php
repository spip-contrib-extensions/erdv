<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin
 *
 * @plugin     Erdv
 * @copyright  2024
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Erdv\Installation
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


/**
 * Fonction d'installation et de mise à jour du plugin.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
**/
function erdv_upgrade($nom_meta_base_version, $version_cible) {
	$maj = [];

	$maj['create'] = [
		['maj_tables', ['spip_erdvs','spip_erdvs_liens','spip_ecalendriers','spip_ecalendriers_liens']], 
		['ecrire_config','erdv/delai_pour_anonymiser', '-3 month'],
		['ecrire_config','erdv/delai_pour_archiver', '-1 month'],
		['ecrire_config','erdv/duree_du_rdv', '+1 hour'],
	];

	// permettre de définir une class pour chaque rendez-vous
	$maj['1.0.2'] = [
		['sql_alter', 'TABLE spip_erdvs ADD COLUMN class varchar(255) NOT NULL DEFAULT "non" AFTER description'],
	];

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Fonction de désinstallation du plugin.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
**/
function erdv_vider_tables($nom_meta_base_version) {
	# suppression des tables
	sql_drop_table('spip_erdvs');
	sql_drop_table('spip_erdvs_liens');
	sql_drop_table('spip_ecalendriers');
	sql_drop_table('spip_ecalendriers_liens');
	
	effacer_config("erdv");
	effacer_meta($nom_meta_base_version);
}