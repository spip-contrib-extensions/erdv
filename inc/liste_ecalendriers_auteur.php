<?php
/**
 * Fonction donnant la liste des calendriers auxquels un auteur à accès
 *
 * @exemple
 * ```
 * 	if ($calendriers = charger_fonction('liste_ecalendriers_auteur','inc', true)) {
 *		echo print_r($calendriers($id_auteur), true);
 *	}
 * ```
 *
 * @plugin     erdv
 * @copyright  2024
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Erdv\x
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction retournant la liste des calendriers auxquels l'auteur a accès
 * 
 * @param int $id_auteur identifiant unique de l'auteur
 * @return array liste des identifiants uniques des calendriers
**/
function inc_liste_ecalendriers_auteur_dist($id_auteur): array {
	$ecalendriers = [];
	// un auteur peut voir tous les calendriers auxquels il est associé
	if (
		include_spip('action/editer_liens')
		&& ($liens = objet_trouver_liens(['id_auteur' => $id_auteur], ['ecalendrier' => '*']))
	){
		$cal_auteur = array_column($liens, 'ecalendrier');
		$ecalendriers = array_merge($cal_auteur, $ecalendriers);
	}
	// Si le plugin Contacts et Organisations est actif
	// et que les organisations ont été configurées comme associables aux calendriers
	// accroitre la liste des calendriers avec ceux nécessaires.
	if (
		test_plugin_actif('contacts')
		&& include_spip('inc/config')
		&& in_array('spip_ecalendriers', lire_config('contacts_et_organisations/lier_organisations_objets', []))
	) {
		// l'auteur est-il une organisation ? Et dans l'affirmative est-elle associée à un calendrier ?
		if ($id_organisation = sql_getfetsel('id_organisation', 'spip_organisations', 'id_auteur = '.intval($id_auteur))){
			$liens = objet_trouver_liens(['id_organisation' => $id_organisation], ['ecalendrier' => '*']);
			$cal_org = array_column($liens, 'ecalendrier');
			$ecalendriers = array_merge($cal_org, $ecalendriers);
		}
		// l'auteur est-il le contact d'une organisation associée à un calendrier ?
		if ($id_contact = sql_getfetsel('id_contact', 'spip_contacts', 'id_auteur = '.intval($id_auteur))){
			$liens = objet_trouver_liens(['id_organisation' => '*'], ['ecalendrier' => '*','contact'=> $id_contact]);
			$cal_org = array_column($liens, 'ecalendrier');
			$ecalendriers = array_merge($cal_org, $ecalendriers);
		}
	}
	// dédoublonner les valeurs en cas de besoin
	$ecalendriers = array_unique($ecalendriers);
	// placer le calendrier préféré comme la première clé du tableau
	// (qui sera l'onglet ouvert par défaut)
	if (isset($GLOBALS['visiteur_session']['prefs']['defaut_calendrier'])
		&& $GLOBALS['visiteur_session']['id_auteur'] == $id_auteur
		&& !empty($ecalendriers)
		&& in_array($GLOBALS['visiteur_session']['prefs']['defaut_calendrier'], $ecalendriers)
	){
		// quelle est la première clé du tableau ?
		$key_first = array_key_first($ecalendriers);
		// quelle est la clé du calendrier préféré
		$key_search = array_search($GLOBALS['visiteur_session']['prefs']['defaut_calendrier'], $ecalendriers);
		// est-ce que la valeur est différente à celle du calendrier préféré ?
		if ($key_first != $key_search) {
			// on intervertit
			$ecalendriers[$key_search] = $ecalendriers[$key_first];
			$ecalendriers[$key_first] = $GLOBALS['visiteur_session']['prefs']['defaut_calendrier'];
		}
	}
	return $ecalendriers;
}
