<?php

/**
 * Une API pour erdv
 *
 * API à utiliser :
 *	 ```
 *	 include_spip('inc/erdv');
 *	 ```
 * @plugin	 Erdv
 * @copyright  2024
 * @author	 Vincent CALLIES
 * @licence	GNU
 * @package	SPIP\Erdv\inc
 */

/**
 * Test de sécurité pour vérifier que SPIP est chargé 
 * et donc que les fichiers ne sont pas appelés en dehors de l'usage de SPIP
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Classe SemaineEnCours
 * 
 * Cette classe permet de calculer les dates des jours de la semaine en cours
 * à partir d'un timestamp donné.
 *
 * Utilisation de la classe
 * ```
 * $semaineEnCours = new SemaineEnCours(time());
 * print_r($semaineEnCours->getSemaine());
 * ```
 */
class SemaineEnCours {
	/**
	 * @var int $time Timestamp utilisé pour calculer les dates de la semaine en cours.
	 */
	private $time;

	/**
	 * Constructeur de la classe SemaineEnCours.
	 * 
	 * @param int $time Timestamp initial.
	 */
	public function __construct(int $time) {
		$this->time = $time;
	}

	/**
	 * Retourne un tableau contenant les dates des jours de la semaine en cours.
	 * 
	 * @return array Tableau des dates des jours de la semaine en cours au format 'Y-m-d H:i:s'.
	 */
	public function getSemaine(): array {
		$semaine = [];
		// quel jour sommes-nous ? N donne 1 pour Lundi à 7 pour Dimanche
		$dj = date('N', $this->time);

		for ($i = 1; $i <= 7; $i++) {
			$daysDifference = $i - $dj;
			$spectre = date('Y-m-d', strtotime("$daysDifference days", $this->time)) . ' 00:00:00';
			$semaine[] = $spectre;
		}

		return $semaine;
	}
}

class RendezVousFetcher {
	/**
	 * Récupère les rendez-vous pour une semaine donnée.
	 *
	 * @param array $semaine Tableau contenant les dates de la semaine.
	 * @param int $id_ecalendrier Identifiant du calendrier.
	 * @return array Tableau contenant les rendez-vous.
	 */
	public function recuperer(array $semaine, int $id_ecalendrier) : array {
		$date_debut = sql_quote(date('Y-m-d', strtotime($semaine[0])));
		$date_fin = sql_quote(date('Y-m-d', strtotime($semaine[6])));
		return sql_allfetsel('*', 'spip_erdvs', [
			'id_ecalendrier=' . intval($id_ecalendrier),
			sql_in('statut', ['publie']),
			'toute_la_journee=' . sql_quote('oui'),
			"DATE(date_debut) <= $date_fin",
			"DATE(date_fin) >= $date_debut",
		], '', 'date_debut ASC');
	}
}

class RendezVousOrganizer {
	/**
	 * Organise les rendez-vous sur une semaine donnée en fonction de leur récurrence et de leur durée.
	 *
	 * @param array $semaine Tableau contenant les dates de la semaine.
	 * @param array $liens Tableau contenant les rendez-vous.
	 * @param string|null $class Classe CSS optionnelle pour styliser les éléments.
	 * @return array Tableau contenant les rendez-vous organisés par jour.
	 */
	public function organiser(array $semaine, array $liens, ?string $class = 'type') : array {
		$lignes = [];
		$elements = [];
		$numero_de_la_semaine = date('W', strtotime($semaine[0]));

		foreach ($liens as $i => $l) {
			$l['j'] = date('N', strtotime($l['date_debut'])) - 1;
			$l['k'] = date('N', strtotime($l['date_fin'])) - 1;
			$l = $this->ajusterJours($l, $numero_de_la_semaine);
			// Utilisation de la classe
			$analyseRecurrence = new AnalyseRecurrence();
			if ($affiche = $analyseRecurrence->analyser($l, $semaine)) {
				$l['colspan'] = ($l['k'] + 1) - $l['j'];
				$elements[$i] = $this->marquerJoursOccupes($l);
			}
		}
		// Nous utilisons l'opérateur <=> pour comparer $colspanB et $colspanA,
		// ce qui trie les éléments en ordre décroissant de colspan.
		usort($elements, function($a, $b) {
			$colspanA = isset($a[0]['colspan']) ? $a[0]['colspan'] : 0;
			$colspanB = isset($b[0]['colspan']) ? $b[0]['colspan'] : 0;
			return $colspanB <=> $colspanA;
		});
		$lignes = $this->organiserLignes($elements);

		if (!empty($lignes) && strlen($class)) {
			$lignes[0][-1] = ['class' => $class];
		}

		return $lignes;
	}

	/**
	 * Ajuste les jours de début et de fin des rendez-vous si nécessaire.
	 *
	 * @param array $l Tableau contenant les informations d'un rendez-vous.
	 * @param int $numero_de_la_semaine Numéro de la semaine actuelle.
	 * @return array Tableau mis à jour avec les jours ajustés.
	 */
	private function ajusterJours(array $l, int $numero_de_la_semaine) : array {
		if (date('W', strtotime($l['date_debut'])) < $numero_de_la_semaine) {
			$l['j'] = 0;
		}
		if (date('W', strtotime($l['date_fin'])) > $numero_de_la_semaine) {
			$l['k'] = 6;
		}
		return $l;
	}

	/**
	 * Marque les jours occupés par un rendez-vous.
	 *
	 * @param array $l Tableau contenant les informations d'un rendez-vous.
	 * @return array Tableau contenant les jours marqués comme occupés.
	 */
	private function marquerJoursOccupes(array $l) : array {
		$elements = [];
		$elements[$l['j']] = [
			'colspan' => $l['colspan'],
			'id_objet' => $l['id_erdv'],
			'objet' => 'erdv',
			'titre' => $l['titre'],
			'class' => $l['class'],
		];
		if ($l['colspan'] > 1) {
			for ($j = 1; $j < $l['colspan']; $j++) {
				$elements[$l['j'] + $j] = 'occupe';
			}
		}
		return $elements;
	}

	/**
	 * Organise les rendez-vous dans des lignes en fonction de leur compatibilité.
	 *
	 * @param array $elements Tableau contenant les rendez-vous à organiser.
	 * @return array Tableau contenant les rendez-vous organisés par ligne.
	 */
	private function organiserLignes(array $elements) : array {
		$lignes = [];
		$i = 0;
		while (!empty($elements)) {
			$lignes[$i] = array_shift($elements);
			$combinaison = array_diff(range(0, 6), array_keys($lignes[$i]));
			foreach ($elements as $key => $values) {
				foreach ($values as $jour => $infos) {
					if (in_array($jour, $combinaison) && is_array($infos) && !isset($lignes[$i][$jour]) && $infos['colspan'] <= count($combinaison)) {
						$lignes[$i][$jour] = $infos;
						unset($elements[$key]);
						if ($infos['colspan'] > 1) {
							for ($add = 1; $add < $infos['colspan']; $add++) {
								$lignes[$i][$jour + $add] = 'occupe';
							}
						}
					}
				}
			}
			$i++;
		}
		return $lignes;
	}
}

class TetrisJournees {
	private $fetcher;
	private $organizer;

	public function __construct() {
		$this->fetcher = new RendezVousFetcher();
		$this->organizer = new RendezVousOrganizer();
	}

	/**
	 * Récupère et organise les rendez-vous sur une semaine donnée.
	 *
	 * @param array $semaine Tableau contenant les dates de la semaine.
	 * @param int $id_ecalendrier Identifiant du calendrier.
	 * @param string|null $class Classe CSS optionnelle pour styliser les éléments.
	 * @return array Tableau contenant les rendez-vous organisés par jour.
	 */
	public function tetris(array $semaine, int $id_ecalendrier, ?string $class = 'type') : array {
		// Récupération des rendez-vous
		$liens = $this->fetcher->recuperer($semaine, $id_ecalendrier);
		
		// Organisation des rendez-vous
		return $this->organizer->organiser($semaine, $liens, $class);
	}
}

/**
 * Classe TetrisHeures
 * 
 * Cette classe permet de consolider les rendez-vous par heure en colonne.
 */
class TetrisHeures {
	/**
	 * Consolider les rendez-vous par heure.
	 * 
	 * La méthode permet de consolider les rendez-vous par heure en colonne.
	 * 
	 * @param array $semaine Tableau des jours de la semaine.
	 * @param int $id_ecalendrier Identifiant du calendrier.
	 * @param string|null $class Classe CSS optionnelle.
	 * @return array Tableau consolidé des rendez-vous.
	 */
	public function consolider(array $semaine, int $id_ecalendrier, ?string $class = 'type'): array {
		$lignes = [];
		$numero_de_la_semaine = date('W', strtotime($semaine[0]));

		$liens = $this->fetchLiens($semaine, $id_ecalendrier);

		if ($liens) {
			foreach ($liens as $l) {
				$this->processLien($l, $semaine, $numero_de_la_semaine, $lignes);
			}

			if (!empty($lignes) && strlen($class)) {
				$lignes[0][-1] = ['class' => $class];
			}
		}

		return $lignes;
	}

	/**
	 * Récupère les liens de rendez-vous depuis la base de données.
	 * 
	 * @param array $semaine Tableau des jours de la semaine.
	 * @param int $id_ecalendrier Identifiant du calendrier.
	 * @return array Tableau des liens de rendez-vous.
	 */
	private function fetchLiens(array $semaine, int $id_ecalendrier): array {
		$conditions = [
			'id_ecalendrier=' . intval($id_ecalendrier),
			sql_in('statut', ['publie']),
			'toute_la_journee=' . sql_quote('non'),
			'DATE(date_debut) <= ' . sql_quote(date('Y-m-d', strtotime($semaine[6]))),
			'DATE(date_fin) >= ' . sql_quote(date('Y-m-d', strtotime($semaine[0]))),
		];

		return sql_allfetsel('*', 'spip_erdvs', $conditions, '', 'DATE_FORMAT(date_debut, "%H") ASC, titre ASC');
	}


	/**
	 * Traite un lien de rendez-vous pour le consolider dans le tableau des lignes.
	 * 
	 * @param array $l Lien de rendez-vous.
	 * @param array $semaine Tableau des jours de la semaine.
	 * @param int $numero_de_la_semaine Numéro de la semaine.
	 * @param array &$lignes Référence au tableau des lignes consolidées.
	 */
	private function processLien(array &$l, array $semaine, int $numero_de_la_semaine, array &$lignes): void {
		$l['j'] = date('N', strtotime($l['date_debut'])) - 1;
		$l['k'] = date('N', strtotime($l['date_fin'])) - 1;

		if (date('W', strtotime($l['date_debut'])) < $numero_de_la_semaine) {
			$l['j'] = 0;
		}
		if (date('W', strtotime($l['date_fin'])) > $numero_de_la_semaine) {
			$l['k'] = 6;
		}

		$analyseRecurrence = new AnalyseRecurrence();
		if ($analyseRecurrence->analyser($l, $semaine)) {
			$this->insertLien($l, $lignes);
		}
	}

	/**
	 * Insère un lien de rendez-vous dans le tableau des lignes consolidées.
	 * 
	 * @param array $l Lien de rendez-vous.
	 * @param array &$lignes Référence au tableau des lignes consolidées.
	 */
	private function insertLien(array $l, array &$lignes): void {
		$l['colspan'] = ($l['k'] + 1) - $l['j'];
		$i = 0;

		while (isset($lignes[$i][$l['j']])) {
			$i++;
		}

		if ($l['colspan'] > 1) {
			for ($verif = $l['j']; $verif < ($l['j'] + $l['colspan']); $verif++) {
				while (isset($lignes[$i][$verif])) {
					$i++;
				}
			}
		}

		$heure_debut = date('H:i', strtotime($l['date_debut']));
		$heure_fin = date('H:i', strtotime($l['date_fin']));
		if ($l['colspan'] > 1) {
			$j_debut = 'date_jour_' . ($l['j'] + 2) . '_abbr';
			$k_fin = $l['k'] + 2;
			if ($k_fin > 7) $k_fin = 1;
			$k_fin = "date_jour_{$k_fin}_abbr";
			$heure_debut = _T($j_debut) . ' ' . $heure_debut;
			$heure_fin = _T($k_fin) . ' ' . $heure_fin;
		}

		$lignes[$i][$l['j']] = [
			'heure_debut' => $heure_debut,
			'heure_fin' => $heure_fin,
			'colspan' => $l['colspan'],
			'id_objet' => $l['id_erdv'],
			'objet' => 'erdv',
			'titre' => $l['titre'],
			'class' => $l['class'],
		];

		if ($l['colspan'] > 1) {
			for ($j = 1; $j < $l['colspan']; $j++) {
				$lignes[$i][$l['j'] + $j] = 'occupe';
			}
		}
	}
}

/**
 * Classe AnalyseRecurrence
 * 
 * Cette classe permet d'analyser la récurrence d'un rendez-vous en fonction des jours de la semaine.
 */
class AnalyseRecurrence {
	/**
	 * Analyse la récurrence d'un rendez-vous.
	 * 
	 * @param array $l Référence à un tableau contenant les informations de récurrence.
	 * @param array $semaine Tableau des jours de la semaine.
	 * @return bool Retourne TRUE si le rendez-vous est récurrent, FALSE sinon.
	 */
	public function analyser(array &$l, array $semaine): bool {
		if ($l['recurrence_jour'] > 0) {
			$jr = $l['recurrence_jour'] - 1;
			$l['j'] = $l['k'] = $jr;

			if ($this->isJourRecurrentSemaine($l, $semaine, $jr) === false) {
				return false;
			}

			if ($this->isJourRecurrentMois($l, $semaine, $jr) === false) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Vérifie si le jour est récurrent sur une semaine.
	 * 
	 * @param array $l Référence à un tableau contenant les informations de récurrence.
	 * @param array $semaine Tableau des jours de la semaine.
	 * @param int $jr Index du jour récurrent.
	 * @return bool Retourne TRUE si le jour est récurrent sur la semaine, FALSE sinon.
	 */
	private function isJourRecurrentSemaine(array $l, array $semaine, int $jr): bool {
		if ($l['recurrence_semaine'] > 0 && $l['recurrence_semaine'] < 6) {
			$sr = date("W", strtotime($semaine[$jr])) - date("W", strtotime(date("Y-m-01"))) + 1;
			return $sr == $l['recurrence_semaine'];
		}
		return true;
	}

	/**
	 * Vérifie si le jour est récurrent sur le mois.
	 * 
	 * @param array $l Référence à un tableau contenant les informations de récurrence.
	 * @param array $semaine Tableau des jours de la semaine.
	 * @param int $jr Index du jour récurrent.
	 * @return bool Retourne TRUE si le jour est récurrent sur le mois, FALSE sinon.
	 */
	private function isJourRecurrentMois(array $l, array $semaine, int $jr): bool {
		if ($l['recurrence_semaine'] > 5) {
			$time = mktime(0, 0, 0, date('n', strtotime($semaine[$jr])), 1, date('Y', strtotime($semaine[$jr])));
			$nom = date('l', strtotime($semaine[$jr]));
			$jour = [
				6 => strtotime($nom, $time),
				7 => strtotime("next $nom", strtotime($nom, $time)),
				8 => strtotime("next $nom", strtotime("next $nom", strtotime($nom, $time))),
				9 => strtotime("next $nom", strtotime("next $nom", strtotime("next $nom", strtotime($nom, $time))))
			];

			return date('n', $jour[$l['recurrence_semaine']]) == date('n', strtotime($semaine[$jr])) && $jour[$l['recurrence_semaine']] == strtotime($semaine[$jr]);
		}
		return true;
	}
}

/**
 * Classe ShakeItUp
 * 
 * Cette classe permet de consolider les rendez-vous préformatés en les insérant
 * dans les colonnes libres des lignes existantes.
 */
class ShakeItUp {
	/**
	 * Consolider les rendez-vous préformatés.
	 * 
	 * La méthode permet, lorsque vous ajoutez des rendez-vous à une liste
	 * existante, de les réinsérer non pas en lignes supplémentaires, mais
	 * dans les colonnes libres des lignes au-dessus.
	 * 
	 * @param array $tableau Tableau des rendez-vous.
	 * @return array Tableau consolidé.
	 */
	public function consolider(array $tableau): array {
		$_tableau = [];
		$l2 = 0;

		foreach ($tableau as $jours) {
			$l = $l2;
			foreach ($jours as $j => $valeurs) {
				// cas des valeurs particulières (ex : generer_saut)
				if (is_string($valeurs)) {
					$_tableau[$l][$j] = $valeurs;
					$l2++;
				} else {
					// si la journée est déjà prise sur la ligne, on va en dessous
					while (isset($_tableau[$l][$j])) {
						$l++;
					}
					// on place la journée à l'emplacement libre
					$_tableau[$l][$j] = $valeurs;
				}
			}
		}

		return $_tableau;
	}
}